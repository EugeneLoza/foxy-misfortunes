# Foxy Misfortunes

A simple top-down shooter game made in Castle Game Engine for Lewdie Jam, 2021.

## Content warning

This game contains material suitable only for mature audience (cartoon nudity, mild suggestive themes). You can play this game only if you are a legal adult.

Optional censored mode (off by default) removes the unnecessary detail from the graphics but doesn't change the mature nature of the game.

## Story

_Follow the (mis)adventures of Foxy and her fellow students from the College of Magic who decided to test their skills, were curious or simply silly and by chance have gotten themselves into embarrassing situations leaving them significantly less dressed than it is socially acceptable._

## Gameplay

Use WASD or arrow keys to move. On Android there is a touch controller in the left bottom of the screen.

Click/tap the map to shoot in that direction. Each shot costs mana, the character's mana is displayed on top of the screen.

ESC pauses the game, there's also an on-screen button. F11 toggles full screen mode, F5 makes a screenshot.

Picking up coins, powerups and killing monsters increases the score. The goal of the game is to earn as many points as possible. Each character has individual high score.

There are many types of monsters and powerups available in-game, from time to time a tutorial message pops up with additional information.

Clothes are hit points. When the character is totally naked it's game over.

Hitting a specific high score unlocks new characters. Reaching even higher score unlocks new skins for the current character. Only one item can be unlocked at a time. Check the character's bio to see how many points are needed to achieve the next goal.

## Installation

### Windows

No installation required - just extract the game into one folder and play.

### Linux

No installation required - just extract the game into one folder and play.

Sometimes you might also need to set "executable" flag for the binary (`chmod +x foxy-misfortunes` or through file properties in the file manager).

You need the following libraries installed to play the game:

* libopenal1
* libpng
* zlib1g
* libvorbis
* libfreetype6
* You also need X-system, GTK at least version 2 and OpenGL drivers for your videocard, however, you are very likely to have those already installed :)

Alternatively on Debian-based systems like Ubuntu you can download the DEB package and install it through a package manager or by `dpkg -i foxy-misfortunes-*-linux-x86_64.deb` command.

### Android

Download and install the APK. The game requires Android 4.2 and higher.

You may need to allow installation of third-party APKs in phone/tablet Settings.

## Support the development

If you like the game and want more content like this - play the game and have fun! As simple as that! The download count alone is something that helps me to keep going. Perhaps even spread the word around social networks to help more people have fun?

In case you want to help even more - leave a good comment at the game's page. It's free and shouldn't take you long, but you really can't overestimate what miracles can player feedback do to my motivation :) Did you like the game? Why not tell about it? Or maybe you see something that can be improved? - I can't promise to implement every requested feature (there are limits to what can be done within a reasonable amount of time), but will most certainly consider it.

And if you really want to be more eloquent and believe that merely saying "thank you" is not enough... No, I don't have a Patreon, at least not yet, but I highly encourage you to consider supporting Castle Game Engine https://www.patreon.com/castleengine . If it weren't for this Game Engine this game would have never existed. And thanks to this Engine I've gotten my full-time dream job as a game developer, so by supporting it you are indirectly financially supporting me too ;)

## Credits

Game by EugeneLoza

Created in Castle Game Engine (https://castle-engine.io/)

Music by Ivan Stanton (northivanastan) and Ted Kerr (Wolfgang)

Sound by SubspaceAudio, altfuture, uEffects and reg7783

Character art and story by EugeneLoza

UI by pzUH

Map tileset by pzUH, StudioFibonacci, sixsixfive, rMiszczak, bocian, Arvin61r58 et. al.

Fonts by Wojciech Kalinowski and Peikos

## Links

Download the latest version: https://decoherence.itch.io/foxy-misfortunes [Windows, Linux, Android]

Source code: https://gitlab.com/EugeneLoza/foxy-misfortunes (GPLv3)
