[Others] Foxy Misfortunes [2.0.721] [EugeneLoza]

[CENTER](COVER ART; DELETE THIS)

[B]Overview:[/B]
Follow the (mis)adventures of Foxy and her fellow students from the College of Magic who decided to test their skills, were curious or simply silly and by chance have gotten themselves into embarrassing situations leaving them significantly less dressed than it is socially acceptable.[/CENTER]

[B]Thread Updated[/B]: 2022-01-08
[B]Release Date[/B]: 2021-10-31
[B]Developer[/B]: eugenezoza [URL="https://decoherence.itch.io/foxy-misfortunes"]itch.io[/URL]
[B]Censored[/B]: NO (Optionally can be turned on in options)
[B]Version[/B]: 2.0.721
[B]OS[/B]: WINDOWS, LINUX, ANDROID
[B]Language[/B]: English
[B]Genre[/B]:
[SPOILER]
2D Game, adventure, Female Protagonist, Mobile Game, Multiple Protagonist, Furry, No Sexual Content, Shooter
[/SPOILER]

[B]Installation[/B]:
[SPOILER]
[B]Windows[/B]
No installation required - just extract the game into one folder and play.
Known bug on Windows 10: Avoid putting the game into a folder with non-ASCII characters if your user name also contains non-ASCII characters.
[B]Linux[/B]
No installation required - just extract the game into one folder and play.
Sometimes you might also need to set "executable" flag for the binary (`chmod +x foxy-misfortunes` or through file properties in the file manager).
You need the following libraries installed to play the game:
* libopenal1
* libpng
* zlib1g
* libvorbis
* libfreetype6
* You also need X-system, GTK at least version 2 and OpenGL drivers for your videocard, however, you are very likely to have those already installed :)
Alternatively on Debian-based systems like Ubuntu you can download the DEB package and install it through a package manager or by `dpkg -i foxy-misfortunes-*-linux-x86_64.deb` command.
[B]Android[/B]
Download and install the APK. The game requires Android 4.2 and higher.
Note that you may need to allow installation of third-party APKs in phone/tablet Settings.
[/SPOILER]

[B]Changelog[/B]:
[SPOILER]
2.0.721
* Fix broken design in Game Over screen
* Finish proof-reading

1.11.711
* Splash screen while the game is loading.
* Fix bug with "Game Won" flag not always saved properly.
* Increase Cheety's rewards a bit.

1.11.694
* Added end-game mechanics and story, updated UI to support winning the game and special music.
* Show more clear information on how to unlock characters and skins.
* Almost finished proof-reading of all the texts in game, there were a lot of fixes :)
* Squirrly now has frost resistance (the effect wears off twice as fast) and Deery has mana lock resistance (variable with the amount of clothes she's wearing - the effect wears twice faster when she's naked)
* Fix UI shaking randomization and optimize it a bit
* Fix bug on Android with shooting and moving simultaneously introduced in the previous build

1.10.669
* "Screen Shake" effects for Lizzy's and Wolfy's explosions.
* Shake effect when heroine loses clothes.
* Option to turn the shaking effect off.
* Increased area sensitive to clicks/taps - now it's easier to shoot near the map edges.

1.10.658
* Fix alpha bleeding on Lizzy - Cobra skin

1.10.656
* New playable character: Lizzy - with her art (3 skins), personal soundtrack, sounds, UI, game mechanics and story.
* Fix alpha bleeding bug. This beast took most of the development time, but now finally we don't have dark borders around powerups and monsters, and no more dark lines on playable characters portraits. Huge thanks @michaliskambi (author of Castle Game Engine) for explanation and help with this issue!
* Shifted images mechanics. This was not necessary for the current game (we didn't run low on video memory anywhere), but is important for further projects. Still gives a tiny performance and VRAM boost.
* Fix tileset - 2 tiles were semi-transparent when they shouldn't have been.
* Smaller score to unlock new skins - it was way too hard, especially on mobile.
* Started proof-reading all in-game texts (not yet finished, but a lot of small improvements already implemented).
* Fix Cheety - Cloud skin colors.
* Fix Foxy - Arctic skin geometry.
* Small fixes and improvements.
* Ubuntu/Debian packaging

1.9.588
* Mechanics for different skins that are unlocked by reaching a specific score.
* Graphics for 5 character's skins (7 new skins total), some minor fixes to existing skins:
* - Foxy - 2 skins
* - Cheety - 3 skins
* - Wolfy - 2 skins
* - Squirrly - 2 skins
* - Deery - 3 skins
* A lot of UI fixes and reorganizations
* Rework tileset for new skins and add a few more tiles to map
* Play sound when showing tutorial
* Prepare some features for the last character
* Other improvements and fixes

1.8.509
* Tutorial now shows information on all important in-game events and objects. Note, that it's designed for a fresh game and will get a bit messy if start e.g. with Deery, but some messages will just be a bit late, nothing will break on old saves.
* New monsters graphics and animations! Everything was reworked from the scratch.
* New sounds
* Managed monsters spawns - now only easier monsters spawn earlier
* All tiles now have 1 pixel border around them (fix graphics clipping)
* Monsters bullets: Mana Lock and Freeze
* Map decals: Mana Lock and Freeze
* New map tiles
* Monsters have random sizes and can be randomly flipped horizontally
* Added information on how to support development to itch.io page and readme ;)
* Fix player spell not recharging when frozen
* Fix bullet shift from shooter (prevent large monsters from shooting through walls and fix bug when player could shoot at monster but not hit it on very close range)
* Remove old tutorial
* Fix sounds priorities (when important sounds could accidentally go missing)

1.7.451
* A few fixes and trims to the story (mostly Squirrly's but also for a few other characters)

1.7.445
* New playable character: Deery with her new game mechanics, assets and story.
* New special character type: Skincaster which affects character spell cost, mana pool, movement speed and spell recharge.
* New "on-strip" events, such as bonus shield, mana, speed, spell recharge. Also support for variable score rewards for losing clothes.
* Increase "goal" a bit: 1500 for Foxy; 1600 for Cheety; 1700 for Wolfy; 1800 for Squirrly and 1900 for Deery.
* Rebalance the game a bit - increase monsters spawns for all characters but Foxy.

1.6.422
* New character - Squirrly with her story, assets and game mechanics.
* Fix bullets animations
* Better "character report" - includes all important values; Resign from percents to seconds (of mana regeneration)
* Fix UI to handle characters with different max hit-points
* Some assets for new upcoming characters and enemies
* Re-export "New character unlocked" music track
* Rebalance rewards for Wolfy (reduce them significantly, still higher than for Cheety)

1.5.367
* Increased rewards for Wolfy. Many more golden coins :)
* Golden coins now give 30 points as tutorial mentions, not 25.
* Increased Wolfy's speed from 1.75 -> 1.9. She is still slow, but more agile now.
* Wolfy adds a brief shield when shoots. Also helps prolonging shield received from powerup.
* Reworked the way shield is implemented a bit; now even small values make sense.
* When Wolfy loses clothes she kills all opponents nearby.
* Automatic character summary generation.
* Better map rendering (allow more convenient bullets management)
* Fix background rendering.
* Tile renderer (for Tutorial in future updates)

1.5.350
* New unlockable playable character Wolfy with her story and assets.
* Homing bullets!
* If the player character is out of mana, it doesn't reset recharge of the spell.
* Backwards compatibility - if you have gained a score as Cheety higher than required to unlock Wolfy, you'll simply need to earn more than 1500/750 score as Cheety again to properly unlock Wolfy, not necessary to beat your old highscore. No need to reset save game.
* Re-export Foxy to get proper colors without post-processing which broke anti-aliasing and made her look non-smooth.
* A few more minor fixes and improvements.

1.4.309
* Cheety character art. Special thanks to chase0w0 for help with colors - hopefully I did those right this time :D
* Rework tileset. 3 more possible walls textures; rework some other textures. Fix Foxy's colors. And of course Cheety's assets.
* Cheety's story, some fixes to Foxy's story and overall game and gameplay description.
* Mechanics for unlockable characters.
* Individual bullets for characters.
* Independent high scores for different characters.
* More sounds for different in-game events.
* Music for unlocking the new character.
* Other small fixes and improvements.

1.3.249
* Mana powerup - restores 50% of mana.
* Shield powerup - doesn't allow enemies to come closer to the Player.
* Speed powerup - faster movement for short duration.
* Different value of coins (bronze, gold and platinum).
* Balancing: return to linear formula, but make it less forgiving.
* Reduce enemies collider sizes (hopefully will improve AI behavior a bit)
* Option to disable showing content warning every time, once is enough :)
* Fix bug of killing multiple enemies with one bullet (there will be such bullets in the future, but for now it was a bug :))
* Also added a temporary "tutorial" - as the amount of objects on map is increasing. It will be significantly improved soon.
* Re-exported game icon in 512x512 for modern devices
* Small optimization, don't make memory reallocation every frame and no loner need in corner workarounds
* Add sounds for all new events in-game
* Dash mechanics
* Dash and charge meters
* Mana lock mechanics
* UI style swapping
* Player frozen state
* Explosive bullets
* Piercing bullets

1.2.213
* Big refactor - the code became much more extensible, allowing implementing different characters, powerups, monsters, etc. in the future
* Variable monsters types:
* - Green slime - slow and mostly harmless
* - Pink slime - on pace with Foxy, average danger
* - Blue slime - tiny, hard to hit and can squeeze through wall corners
* - Red slime - very fast, dangerous
* - Ghost - slow, but can pass through walls
* Fixed bug with enemies passing through corners (left this feature as blue slime's quirk)
* Animations for monsters and bullets
* Brand new tileset texture (still experimental)
* Different maps tilesets
* Multiple minor fixes and improvements

1.1.171
* Rebalance difficulty curve. Now aiming at 5-6 minutes run, not 10-20. (Game becomes more difficult faster)
* Show story in-game.
* Button to reset game, if you want to get rid of high score that was achieved when the game was easier.
* Pause game by ESC key or on-screen button
* Fix repeating music bug
* Small UI tweaks and other minor improvements

1.1.143
* Variable map size
* On Android default map is now smaller
* Larger bullets - it's easier to hit enemies now
* Small fixes and fine-tuning

1.0.119
* Android version and appropriate fixes and improvements

1.0.103
* Added missing options and menus
* Fixed some glitches in monsters AI
* Significant balance changes
* Reduce random influence

0.9.72
* AI fixes
* Gameplay polishing
* A few minor improvements

0.8.66
* Fix saving highscores

0.8.60
Original version of the game submitted for Lewdie Jam, made in 12 hours (7 for development, 5 for art) :)
[/SPOILER]

[B]Developer Notes[/B]:
[SPOILER]
A simple silly top-down shooter game where clothes are hit-points.
Made in Castle Game Engine for Lewdie Jam.
[/SPOILER]

[CENTER][B][SIZE=6]DOWNLOAD[/SIZE][/B]
[SIZE=5][B]Win[/B]: MIRROR - MIRROR - MIRROR
[B]Linux[/B]: MIRROR - MIRROR - MIRROR
[B]Others[/B]: ANDROID

(SAMPLES/SCREENSHOTS; DELETE THIS)[/CENTER]