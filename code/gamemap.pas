{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameMap;

interface

uses
  Classes, Generics.Collections,
  CastleUiControls, CastleControls, CastleGlImages, CastleKeysMouse, CastleRectangles;

const
  MaxSizeX = 30;
  MaxSizeY = 23;

type
  TMapInfo = Boolean;

type
  TEntity = class(TObject)
  public
    Kind: Integer;
    X, Y: Single;
  end;
  TMonster = class(TEntity)
  public
    TargetX, TargetY: Single;
    IsBuggy: Single;
    AnimationTime: Single;
    IsAlive: Boolean;
    Frozen: Single;
    Inverted: Boolean;
    ActionTimeout: Single;
    RandomSize: Single;
  end;
  TMonstersList = specialize TObjectList<TMonster>;
  TBullet = class(TEntity)
  public
    DX, DY: Single;
    AnimationTime: Single;
    IsAlive: Boolean;
  end;
  TBulletsList = specialize TObjectList<TBullet>;
  TPowerup = TEntity;
  TPowerupsList = specialize TObjectList<TPowerup>;
  TDecal = class(TEntity)
  public
    Timeout: Single;
    RandomSize: Single;
  end;
  TDecalsList = specialize TObjectList<TDecal>;
  TPlayer = class(TEntity)
  public
    IsFrozen: Single;
    ManaLock: Single;
    Shield: Single;
    Dash: Single;
    //for dashing through the walls
    DashCharge: Single;
    SpeedBoost: Single;
  end;

type
  TMap = class(TCastleUserInterface)
  public const MapTilesets = 9;
  strict private
    LastMapKind: Integer;
    MapKind: Integer;
    ScrRects, ImgRects: array of TFloatRectangle;
    function ReadMapSafe(const X, Y: Integer): TMapInfo;
    procedure WriteMapSafe(const X, Y: Integer; const AValue: TMapInfo);
  public
    Texture: TDrawableImage;
    Scale: Single;
    StartX, StartY: Single;
    Powerups: TPowerupsList;
    Monsters: TMonstersList;
    Bullets: TBulletsList;
    Decals: TDecalsList;
    Player: TPlayer;
    SizeX, SizeY: Integer;
    Map: array [0..Pred(MaxSizeX), 0..Pred(MaxSizeY)] of TMapInfo;
    procedure GetMapSize;
    procedure Generate;
    function CanMove(const X, Y, Size: Single): Boolean;
    function ScreenToMapX(const X: Single): Single;
    function ScreenToMapY(const Y: Single): Single;
  public
    procedure Render; override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

implementation
uses
  SysUtils, CastleLog,
  CastleConfig, CastleUtils,
  GameRandom, GameCharacters, GameMonsters, GamePowerups, GameBullets, GameDecals;

type
  TDir = (dUp, dDown, dLeft, dRight);
  TMove = record
    X, Y: Integer;
    Dirs: set of TDir;
  end;
  TMovesList = specialize TList<TMove>;

procedure TMap.GetMapSize;
begin
  SizeX := UserConfig.GetValue('map_size', 15);
  case SizeX of
    30: SizeY := 23;
    22: SizeY := 17;
    19: SizeY := 15;
    15: SizeY := 12;
    13: SizeY := 10;
    else
    begin
      SizeX := 22;
      SizeY := 17;
    end;
  end;
end;
function TMap.ReadMapSafe(const X, Y: Integer): TMapInfo;
begin
  if (X >= 0) and (Y >= 0) and (X < SizeX) and (Y < SizeY) then
    Result := Map[X, Y]
  else
    Result := true;
end;
procedure TMap.WriteMapSafe(const X, Y: Integer; const AValue: TMapInfo);
begin
  if (X >= 0) and (Y >= 0) and (X < SizeX) and (Y < SizeY) then
    Map[X, Y] := AValue;
end;

function TMap.CanMove(const X, Y, Size: Single): Boolean;

  function CanMovePoint(const AX, AY: Single): Boolean;
  begin
    Result := not ReadMapSafe(Trunc(AX), Trunc(AY));
  end;

var
  S: Single;
begin
  S := Size / 2;
  Result := (X >= 0) and (Y >= 0) and //Trunc bug/feature
            CanMovePoint(X, Y) and
            CanMovePoint(X + S, Y) and
            CanMovePoint(X - S, Y) and
            CanMovePoint(X, Y + S) and
            CanMovePoint(X, Y - S) and
            CanMovePoint(X + S/2, Y + S/2) and
            CanMovePoint(X - S/2, Y + S/2) and
            CanMovePoint(X + S/2, Y - S/2) and
            CanMovePoint(X - S/2, Y - S/2);
end;

function TMap.ScreenToMapX(const X: Single): Single;
begin
  Result := (X - StartX) / Scale;
end;
function TMap.ScreenToMapY(const Y: Single): Single;
begin
  Result := (Y - StartY) / Scale;
end;

procedure TMap.Generate;
var
  X, Y: Integer;
  M: TMove;
  MovesList: TMovesList;
  FreeTiles: Integer;

  function CanTunnel(const AX, AY: Integer; out AMove: TMove): Boolean;
  var
    Count: Integer;
  begin
    if not Map[AX, AY] then
      Exit(false);
    Count := 0;
    AMove.Dirs := [];
    AMove.X := AX;
    AMove.Y := AY;
    if ReadMapSafe(AX + 1, AY) then
    begin
      Inc(Count);
      AMove.Dirs += [dRight];
    end;
    if ReadMapSafe(AX - 1, AY) then
    begin
      Inc(Count);
      AMove.Dirs += [dLeft];
    end;
    if ReadMapSafe(AX, AY + 1) then
    begin
      Inc(Count);
      AMove.Dirs += [dUp];
    end;
    if ReadMapSafe(AX, AY - 1) then
    begin
      Inc(Count);
      AMove.Dirs += [dDown];
    end;
    Result := Count = 3;
  end;

  function GetRandomDir(const AMove: TMove): TDir;
  begin
    repeat
      case Rnd.Random(4) of
        0: Result := dUp;
        1: Result := dDown;
        2: Result := dLeft;
        3: Result := dRight;
      end;
    until Result in AMove.Dirs;
  end;

begin
  Monsters.Clear;
  Powerups.Clear;
  Bullets.Clear;
  Decals.Clear;

  if (UserConfig.GetValue('preferred_map_kind', -1) < 0) then
  begin
    repeat
      MapKind := Rnd.Random(MapTilesets);
    until LastMapKind <> MapKind;
    LastMapKind := MapKind;
    MapKind := 31 - MapKind;
  end else
    MapKind := 31 - UserConfig.GetValue('preferred_map_kind', -1);

  MovesList := TMovesList.Create;
  repeat
    for X := 0 to Pred(SizeX) do
      for Y := 0 to Pred(SizeY) do
        Map[X, Y] := true;
    Map[SizeX div 2, SizeY div 2] := false;
    Player.X := SizeX div 2 + 0.5;
    Player.Y := SizeY div 2 + 0.5;
    Player.IsFrozen := -1;
    Player.ManaLock := -1;
    repeat
      MovesList.Clear;
      for X := 0 to Pred(SizeX) do
        for Y := 0 to Pred(SizeY) do
          if CanTunnel(X, Y, M) then
            MovesList.Add(M);
      if MovesList.Count > 0 then
      begin
        M := MovesList[Rnd.Random(MovesList.Count)];
        WriteMapSafe(M.X, M.Y, false);
        case GetRandomDir(M) of
          dLeft: WriteMapSafe(M.X - 1, M.Y, false);
          dRight: WriteMapSafe(M.X + 1, M.Y, false);
          dUp: WriteMapSafe(M.X, M.Y + 1, false);
          dDown: WriteMapSafe(M.X, M.Y - 1, false);
        end;
      end;
    until MovesList.Count = 0;
    FreeTiles := 0;
    for X := 0 to Pred(SizeX) do
      for Y := 0 to Pred(SizeY) do
        if not Map[X, Y] then
          Inc(FreeTiles);
    //WriteLnLog((Single(FreeTiles) / (SizeX * SizeY)).ToString);
  until (Single(FreeTiles) / (SizeX * SizeY) >= 0.5) and (Single(FreeTiles) / (SizeX * SizeY) <= 0.7);
  MovesList.Free;
end;

constructor TMap.Create(AOwner: TComponent);
begin
  inherited;
  FullSize := true;
  Texture := TDrawableImage.Create('castle-data:/map/tileset.png', true);
  Powerups := TPowerupsList.Create(true);
  Decals := TDecalsList.Create(true);
  Monsters := TMonstersList.Create(true);
  Bullets := TBulletsList.Create(true);
  Player := TPlayer.Create;
  LastMapKind := -1;
end;

destructor TMap.Destroy;
begin
  Texture.Free;
  Powerups.Free;
  Decals.Free;
  Monsters.Free;
  Bullets.Free;
  Player.Free;
  inherited
end;

procedure TMap.Render;
var
  Count, MaxCount: Integer;

  function Tile(const AX, AY: Integer): TFloatRectangle;
  begin
    Result := FloatRectangle(AX * 64 + 1, AY * 64 + 1, 64 - 2, 64 - 2);
  end;

  function Frame8(const ATime, ADuration: Single): Integer;
  begin
    Result := 2 + Trunc(8 * FloatModulo(ATime, ADuration));
  end;
  function Frame6(const ATime, ADuration: Single): Integer;
  begin
    Result := 10 + Trunc(6 * ATime / ADuration);
    if Result > 15 then
      Result := 15;
  end;

  procedure RenderDecals;
  var
    I: Integer;
  begin
    for I := 0 to Pred(Decals.Count) do
    begin
      ScrRects[Count] := FloatRectangle(StartX + (Decals[I].X - Decals[I].RandomSize * DecalsInfo[Decals[I].Kind].RenderSize / 2) * Scale, StartY + (Decals[I].Y - Decals[I].RandomSize * DecalsInfo[Decals[I].Kind].RenderSize / 2) * Scale, Scale * Decals[I].RandomSize * DecalsInfo[Decals[I].Kind].RenderSize, Scale * Decals[I].RandomSize * DecalsInfo[Decals[I].Kind].RenderSize);
      ImgRects[Count] := Tile(1, DecalsInfo[Decals[I].Kind].RenderTile);
      Inc(Count);
    end;
  end;

  procedure RenderMap;
  var
    IX, IY: Integer;
  begin
    for IX := 0 to Pred(SizeX) do
      for IY := 0 to Pred(SizeY) do
        if Map[IX, IY] then
        begin
          ScrRects[Count] := FloatRectangle(StartX + IX * Scale, StartY + IY * Scale, Scale, Scale);
          ImgRects[Count] := Tile(0, MapKind);
          Inc(Count);
        end;
  end;

  procedure RenderPowerups;
  var
    I: Integer;
  begin
    for I := 0 to Pred(Powerups.Count) do
    begin
      ScrRects[Count] := FloatRectangle(StartX + (Powerups[I].X - PowerupsInfo[Powerups[I].Kind].RenderSize / 2) * Scale, StartY + (Powerups[I].Y - PowerupsInfo[Powerups[I].Kind].RenderSize / 2) * Scale, Scale * PowerupsInfo[Powerups[I].Kind].RenderSize, Scale * PowerupsInfo[Powerups[I].Kind].RenderSize);
      ImgRects[Count] := Tile(1, PowerupsInfo[Powerups[I].Kind].RenderTile);
      Inc(Count);
    end;
  end;

  procedure RenderMonsters;
  var
    I: Integer;
  begin
    for I := 0 to Pred(Monsters.Count) do
    begin
      ScrRects[Count] := FloatRectangle(StartX + (Monsters[I].X - Monsters[I].RandomSize * MonstersInfo[Monsters[I].Kind].RenderSize / 2) * Scale, StartY + (Monsters[I].Y - Monsters[I].RandomSize * MonstersInfo[Monsters[I].Kind].RenderSize / 2) * Scale, Scale * Monsters[I].RandomSize * MonstersInfo[Monsters[I].Kind].RenderSize, Scale * Monsters[I].RandomSize * MonstersInfo[Monsters[I].Kind].RenderSize);
      if Monsters[I].Inverted then
      begin
        ScrRects[Count].Left += ScrRects[Count].Width;
        ScrRects[Count].Width := -ScrRects[Count].Width;
      end;
      if Monsters[I].IsAlive then
      begin
        ImgRects[Count] := Tile(Frame8(Monsters[I].AnimationTime, 1.0), MonstersInfo[Monsters[I].Kind].RenderTile);
        if Monsters[I].Frozen > 0 then
        begin
          Inc(Count);
          ScrRects[Count] := ScrRects[Count - 1];
          ImgRects[Count] := Tile(1, 22);
        end;
      end else
        ImgRects[Count] := Tile(Frame6(Monsters[I].AnimationTime, DeathAnimationTime), MonstersInfo[Monsters[I].Kind].RenderTile);
      Inc(Count);
    end;
  end;

  procedure RenderBullets;
  var
    I, J, TailsNow: Integer;
    ThisSize, TailSize, TailDelay: Single;
  begin
    for I := 0 to Pred(Bullets.Count) do
    begin
      TailSize := BulletsInfo[Bullets[I].Kind].RenderSize / 3;
      TailDelay := Pred(BulletsInfo[Bullets[I].Kind].Tails) * TailSize / BulletsInfo[Bullets[I].Kind].Speed;
      if Bullets[I].AnimationTime < TailDelay then
        TailsNow := Round(Pred(BulletsInfo[Bullets[I].Kind].Tails) * Bullets[I].AnimationTime / TailDelay)
      else
        TailsNow := Pred(BulletsInfo[Bullets[I].Kind].Tails);
      if Bullets[I].IsAlive then
        for J := TailsNow downto 0 do
        begin
          ThisSize := BulletsInfo[Bullets[I].Kind].RenderSize * 2 / (J + 2);
          ScrRects[Count] := FloatRectangle(StartX + (Bullets[I].X - J * Bullets[I].DX * TailSize - ThisSize / 2) * Scale, StartY + (Bullets[I].Y - J * Bullets[I].DY * TailSize - ThisSize / 2) * Scale, Scale * ThisSize, Scale * ThisSize);
          ImgRects[Count] := Tile(Frame8(Bullets[I].AnimationTime + Single(J) / BulletsInfo[Bullets[I].Kind].Tails, 1.0), BulletsInfo[Bullets[I].Kind].RenderTile);
          Inc(Count);
        end
      else
      begin
        ThisSize := BulletsInfo[Bullets[I].Kind].RenderSize * (1 + BulletsInfo[Bullets[I].Kind].ExplosionRenderSizeMinusOne * Bullets[I].AnimationTime / BulletDeathAnimationTime);
        ScrRects[Count] := FloatRectangle(StartX + (Bullets[I].X - ThisSize / 2) * Scale, StartY + (Bullets[I].Y - ThisSize / 2) * Scale, Scale * ThisSize, Scale * ThisSize);
        ImgRects[Count] := Tile(Frame6(Bullets[I].AnimationTime, BulletDeathAnimationTime), BulletsInfo[Bullets[I].Kind].RenderTile);
        Inc(Count);
      end;
    end;
  end;

  procedure RenderPlayer;
  var
    //for some reason I thought we're doing this for every enemy. But no need to delete this optimization as it's already here
    ShieldSqrt: Single;
  begin
    if Player.Shield > 0 then
    begin
      ShieldSqrt := Sqrt(Player.Shield + 0.2);
      ScrRects[Count] := FloatRectangle(StartX + (Player.X - ShieldSqrt) * Scale, StartY + (Player.Y - ShieldSqrt) * Scale, Scale * ShieldSqrt * 2, Scale * ShieldSqrt * 2);
      ImgRects[Count] := Tile(1, 21);
      Inc(Count);
    end;
    ScrRects[Count] := FloatRectangle(StartX + (Player.X - CharactersInfo[CurrentCharacter].RenderSize / 2) * Scale, StartY + (Player.Y - CharactersInfo[CurrentCharacter].RenderSize / 2) * Scale, Scale * CharactersInfo[CurrentCharacter].RenderSize, Scale * CharactersInfo[CurrentCharacter].RenderSize);
    ImgRects[Count] := Tile(CharactersInfo[CurrentCharacter].Skins[CurrentSkin].RenderTileX, CharactersInfo[CurrentCharacter].Skins[CurrentSkin].RenderTileY);
    Inc(Count);
    if Player.IsFrozen > 0 then
    begin
      ScrRects[Count] := ScrRects[Count - 1];
      ImgRects[Count] := Tile(1, 22);
      Inc(Count);
    end;
  end;

begin
  inherited; // parent is empty
  if RenderRect.Width / SizeX > RenderRect.Height / SizeY then
    Scale := RenderRect.Height / SizeY
  else
    Scale := RenderRect.Width / SizeX;
  StartX := RenderRect.Left + (RenderRect.Width - SizeX * Scale) / 2;
  StartY := RenderRect.Bottom + (RenderRect.Height - SizeY * Scale) / 2;

  // Here we deliberately calculate "the max possible" just to be 100% sure that we won't run out of allocated memory
  MaxCount := SizeX * SizeY + Bullets.Count * MaxTails + Powerups.Count + Decals.Count + Monsters.Count * 2 + 1 * 3; //+1 for Player +1 for shield +1 for snowflake +N for familiar
  if Length(ScrRects) < MaxCount then
  begin
    // Avoid re-allocating memory too often
    SetLength(ScrRects, MaxCount);
    SetLength(ImgRects, MaxCount);
  end;

  Count := 0;

  RenderDecals;
  RenderMap;
  //Render entities
  RenderPowerups;
  RenderBullets;
  RenderMonsters;
  //RenderFamiliar
  RenderPlayer;
  Texture.Draw(@ScrRects[0], @ImgRects[0], Count);
end;

end.

