{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameBullets;

{$mode ObjFPC}{$H+}

interface

uses
  Generics.Collections, DOM;

type
  TAffectsPlayer = (apUndefined, apNone, apFreeze, apManaDrain);

const
  MaxTails = 5;
  BulletDeathAnimationTime = 0.2;

type
  TBulletInfo = class(TObject)
  strict private
  public
    Id: Integer;
    ImpactSound: String;
    RenderTile: Integer;
    RenderSize: Single;
    InteractionSize: Single;
    CollisionSize: Single;
    //Explosion always affects only monsters
    ExplosionRenderSizeMinusOne: Single;
    { negative means explosion only visual }
    ExplosionInteractionSize: Single;
    ExplosionFreeze: Single;
    ExplosionClearsDecals: Boolean;
    ExplosionDecal: Integer;
    AffectsPlayer: TAffectsPlayer;
    KillsMonster: Boolean;
    Speed: Single;
    Tails: Integer;
    IsPiercing: Boolean;
    Homing: Single;
    Shake: Single;
    constructor Create(const Element: TDOMElement);
  end;
  TBulletsList = specialize TObjectList<TBulletInfo>;

var
  BulletsInfo: TBulletsList;

procedure InitBullets;

implementation
uses
  SysUtils, CastleXmlUtils;

procedure InitBullets;
var
  NewBullet: TBulletInfo;
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
begin
  BulletsInfo := TBulletsList.Create(true);
  Doc := URLReadXML('castle-data:/map/bullets.xml');
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('bullet');
    try
      while Iterator.GetNext do
      begin
        NewBullet := TBulletInfo.Create(Iterator.Current);
        if NewBullet.Id <> BulletsInfo.Count then
          raise Exception.CreateFmt('Unexpected bullet Id: %d (expected: %d) - All items must have unique IDs identical to their order', [NewBullet.Id, BulletsInfo.Count]);
        BulletsInfo.Add(NewBullet);
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;
end;

constructor TBulletInfo.Create(const Element: TDOMElement);

  function StrToAffectsPlayer(const AString: String): TAffectsPlayer;
  begin
    case AString of
      'apNone': Result := apNone;
      'apFreeze': Result := apFreeze;
      'apManaDrain': Result := apManaDrain;
      else
        raise Exception.Create('Unexpected AffectsPlayer: ' + AString);
    end;
  end;

begin
  //inherited - empty
  Id := Element.AttributeInteger('Id');
  ImpactSound := Element.AttributeString('ImpactSound');
  RenderTile := Element.AttributeInteger('RenderTile');
  RenderSize := Element.AttributeFloat('RenderSize');
  InteractionSize := Element.AttributeFloat('InteractionSize');
  CollisionSize := Element.AttributeFloat('CollisionSize');
  ExplosionRenderSizeMinusOne := Element.AttributeFloat('ExplosionRenderSizeMinusOne');
  ExplosionInteractionSize := Element.AttributeFloat('ExplosionInteractionSize');
  ExplosionFreeze := Element.AttributeFloat('ExplosionFreeze');
  ExplosionClearsDecals := Element.AttributeBoolean('ExplosionClearsDecals');
  ExplosionDecal := Element.AttributeInteger('ExplosionDecal');
  AffectsPlayer := StrToAffectsPlayer(Element.AttributeString('AffectsPlayer'));
  KillsMonster := Element.AttributeBoolean('KillsMonster');
  Speed := Element.AttributeFloat('Speed');
  Tails := Element.AttributeInteger('Tails');
  IsPiercing := Element.AttributeBoolean('IsPiercing');
  Homing := Element.AttributeFloat('Homing');
  Shake := Element.AttributeFloat('Shake');
end;

finalization
  BulletsInfo.Free;
end.

