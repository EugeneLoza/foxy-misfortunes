{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameStateMain;

interface

uses Classes,
  CastleVectors, CastleUIState, CastleComponentSerialize, CastletimeUtils,
  CastleUIControls, CastleControls, CastleKeysMouse, CastleImages,
  TouchControlReimagined,
  CastleShiftedImageControl, CastleUiShaker,
  GameRandom, GameMap, GameTileImage, GameCharacters, GameTutorial;

{$ifdef Android}
{$define TouchController}
{$endif}

const
  BuggyTimeout = 1; // when stuck, monster will go for BuggyTimeout seconds in a random direction

  ManaBarWidth = 630;

type
  { Main state, where most of the application logic takes place. }
  TStateMain = class(TUIState)
  strict private
    MainMenu: TCastleUserInterface;
    ButtonPlay: TCastleButton;
    ButtonOptions: TCastleButton;
    ButtonCredits: TCastleButton;
    ButtonQuit: TCastleButton;
    CreditsMenu: TCastleUserInterface;
    ButtonCreditsMenu: TCastleButton;
    Options: TCastleUserInterface;
    ButtonBackToMainMenu: TCastleButton;
    ContentWarning: TCastleUserInterface;
    ButtonQuitGame: TCastleButton;
    ButtonStartGame: TCastleButton;
    ButtonMapSize: TCastleButton;
    TilesetImage: TTileImage;
    ButtonTileset: TCastleButton;
    ButtonHardcoreMode: TCastleButton;
    ButtonChangeCharacter: TCastleButton;
    LabelCharacterName: TCastleLabel;
    LabelCharacterScore: TCastleLabel;
    ImageCharacterLocked: TCastleUserInterface;
    ButtonChangeCharacterSkin: TCastleButton;
    LabelCharacterSkin: TCastleLabel;
    ImageSkinLocked: TCastleUserInterface;
    Story: TCastleUserInterface;
    StoryLabel: TCastleLabel;
    ScrollViewStory: TCastleScrollView;
    ScrollViewOptions: TCastleScrollView;
    ScrollViewCredits: TCastleScrollView;
    WinGame: TCastleUserInterface;
    ButtonWinGameDone: TCastleButton;
    WinGameScrollView: TCastleScrollView;
    ButtonLetsDoIt: TCastleButton;
    LabelVersion: TCastleLabel;
    procedure DoMainMenu(Sender: TObject);
    procedure ClickOptions(Sender: TObject);
    procedure ClickCredits(Sender: TObject);
    procedure ClickQuit(Sender: TObject);
    procedure ClickContentWarningOk(Sender: TObject);
    procedure ClickMapSize(Sender: TObject);
    procedure ClickHardcoreMode(Sender: TObject);
    procedure ClickTileset(Sender: TObject);
    procedure ClickShowStory(Sender: TObject);
    procedure ClickResetProgress(Sender: TObject);
    procedure ClickConfirmResetProgress(Sender: TObject);
    procedure ClickWinGame(Sender: TObject);
    procedure UpdateMapSizeTitle;
  strict private
    CheatTimer: TTimerResult;
    CheatCount: Integer;
    ButtonCheat: TCastleButton;
    procedure ClickCheat(Sender: TObject);
  strict private
    TutorialTimer: Single;
    IsTutorialBottom: Boolean;
    TutorialBottom: TCastleUserInterface;
    TutorialImage: TTileImage;
    TutorialBottomHeaderLabel, TutorialBottomTextLabel: TCastleLabel;
    TutorialTop: TCastleUserInterface;
    TutorialTopHeaderLabel, TutorialTopTextLabel: TCastleLabel;
    procedure QueueTutorial(const TutorialType: TTutorialType; const TutorialEntry: Integer);
  strict private
    ButtonToggleShakers: TCastleButton;
    ButtonFullscreen: TCastleButton;
    ButtonCensored: TCastleButton;
    ButtonMasterVolume: TCastleButton;
    ButtonMusicVolume: TCastleButton;
    ButtonResetProgress: TCastleButton;
    ConfirmResetProgress: TCastleUserInterface;
    ButtonResetProgressYes, ButtonResetProgressNo: TCastleButton;
    procedure ClickToggleShakers(Sender: TObject);
    procedure ClickFullscreen(Sender: TObject);
    procedure ClickCensored(Sender: TObject);
    procedure ClickMasterVolume(Sender: TObject);
    procedure ClickMusicVolume(Sender: TObject);
    procedure UpdateOptionsCaptions;
  strict private
    ManaBox: TCastleImageControl;
    ManaBar: TCastleImageControl;
    ManaBarLocked: TCastleImageControl;
    ManaDashCircleLeft: TCastleImageControl;
    ManaDashCircleRight: TCastleImageControl;
    ManaChargeCircleLeft: TCastleImageControl;
    ManaChargeCircleRight: TCastleImageControl;
    LavaBox: TCastleImageControl;
    LavaBar: TCastleImageControl;
    LavaBarLocked: TCastleImageControl;
    LavaDashCircleLeft: TCastleImageControl;
    LavaDashCircleRight: TCastleImageControl;
    LavaChargeCircleLeft: TCastleImageControl;
    LavaChargeCircleRight: TCastleImageControl;
    CurrentBar: TCastleImageControl;
    CurrentBarLocked: TCastleImageControl;
    CurrentDashCircleLeft: TCastleImageControl;
    CurrentDashCircleRight: TCastleImageControl;
    CurrentChargeCircleLeft: TCastleImageControl;
    CurrentChargeCircleRight: TCastleImageControl;
  strict private
    {$ifdef TouchController}
    TouchControl: TTouchControlReimagined;
    {$endif}
    GameField: TCastleUiShaker;
    ScoreBox: TCastleUserInterface;
    ScoreLabel: TCastleLabel;
    HighscoreBox: TCastleUserInterface;
    HighscoreLabel: TCastleLabel;
    HighscoreCurrentImage: TcastleUserInterface;
    HighscoreBeforeImage: TcastleUserInterface;
    GameOverGroup: TCastleUserInterface;
    GameOverText: TCastleUserInterface;
    VictoryVerticalGroup: TCastleUserInterface;
    FinalHighscoreImage: TCastleUserInterface;
    FinalScoreLabel: TCastleLabel;
    HighscoreGroup: TCastleUserInterface;
    LabelNewCharacterUnlocked: TCastleLabel;
    LabelNewSkinUnlocked: TCastleLabel;
    LabelUnlockHint: TCastleLabel;
    ButtonAgain: TCastleButton;
    ButtonMenu: TCastleButton;
    ButtonVictory: TCastleButton;
    PauseMenu: TCastleUserInterface;
    ButtonPause: TCastleButton;
    ButtonPauseUnpause, ButtonPauseRestart, ButtonPauseToMainMenu: TCastleButton;
    Map: TMap;
    LastMusic: String;
    procedure StartGame(Sender: TObject);
    procedure ClickPause(Sender: TObject);
  strict private
    CharShaker: TCastleUiShaker;
    CharBase: TCastleShiftedImageControl;
    CharCensored: TCastleShiftedImageControl;
    CharHitpoints: array [0..MaxCharHp] of TCastleShiftedImageControl;
    CharOverlay: TCastleShiftedImageControl;
    HardcoreModeCache: Integer;
    HP: Integer;
    Score: Integer;
    HighScore: Integer;
    GameOn: Boolean;
    TotalTime: Single;
    Mana: Single;
    LastNoManaSound: TTimerResult;
    Recharge: Single;
    MonsterSpawnTimer: Single;
    MonstersToSpawn: Integer;
    PowerupsSpawnTimer: array of Single;
    PowerupsToSpawn: array of Integer;
    procedure HitPlayer;
    procedure UpdateCharClothes;
    procedure SetCurrentCharacter;
    procedure ClickChangeCharacter(Sender: TObject);
    procedure ClickChangeSkin(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single; var HandleInput: Boolean); override;
    {$ifndef Android}
    function Press(const Event: TInputPressRelease): Boolean; override;
    {$endif}
  end;

var
  StateMain: TStateMain;

implementation

uses
  SysUtils,
  {$ifdef Android}CastleOpenDocument,{$endif}
  CastleLog, CastleSoundEngine, CastleConfig, CastleWindow, CastleUtils,
  CastleApplicationProperties, CastleRectangles,
  GameMonsters, GamePowerups, GameBullets, GameDecals;

{ TStateMain ----------------------------------------------------------------- }

constructor TStateMain.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/gamestatemain.castle-user-interface';
end;

procedure TStateMain.Start;
var
  I: Integer;
begin
  inherited;
  Application.MainWindow.FullScreen := UserConfig.GetValue('fullscreen', true);
  SoundEngine.Volume := UserConfig.GetFloat('master_volume', 0.5);
  SoundEngine.LoopingChannel[0].Volume := UserConfig.GetFloat('music_volume', 1.0);
  CurrentCharacter := UserConfig.GetValue('current_character', 0);
  CurrentSkin := UserConfig.GetValue('current_skin_' + CurrentCharacter.ToString, 0);

  Map := TMap.Create(FreeAtStop);

  MainMenu := DesignedComponent('MainMenu') as TCastleUserInterface;
  ButtonPlay := DesignedComponent('ButtonPlay') as TCastleButton;
  ButtonPlay.OnClick := @ClickShowStory;
  ButtonOptions := DesignedComponent('ButtonOptions') as TCastleButton;
  ButtonOptions.OnClick := @ClickOptions;
  ButtonCredits := DesignedComponent('ButtonCredits') as TCastleButton;
  ButtonCredits.OnClick := @ClickCredits;
  ButtonQuit := DesignedComponent('ButtonQuit') as TCastleButton;
  ButtonQuit.OnClick := @ClickQuit;
  ButtonQuit.Exists := not ApplicationProperties.TouchDevice;
  CreditsMenu := DesignedComponent('CreditsMenu') as TCastleUserInterface;
  ButtonCreditsMenu := DesignedComponent('ButtonCreditsMenu') as TCastleButton;
  ButtonCreditsMenu.OnClick := @DoMainMenu;
  ScrollViewStory := DesignedComponent('ScrollViewStory') as TCastleScrollView;
  ScrollViewOptions := DesignedComponent('ScrollViewOptions') as TCastleScrollView;
  ScrollViewCredits := DesignedComponent('ScrollViewCredits') as TCastleScrollView;

  ButtonCheat := DesignedComponent('ButtonCheat') as TCastleButton;
  ButtonCheat.OnClick := @ClickCheat;
  CheatTimer := Timer;

  WinGame := DesignedComponent('WinGame') as TCastleUserInterface;
  ButtonWinGameDone := DesignedComponent('ButtonWinGameDone') as TCastleButton;
  ButtonWinGameDone.OnClick  := @DoMainMenu;
  WinGameScrollView := DesignedComponent('WinGameScrollView') as TCastleScrollView;

  {$ifdef TouchController}
  TouchControl := TTouchControlReimagined.Create(FreeAtStop);
  TouchControl.VerticalAnchorDelta := 20;
  TouchControl.HorizontalAnchorDelta := 160;
  TouchControl.Scale := 2;
  InsertFront(TouchControl);
  {$endif}

  ContentWarning := DesignedComponent('ContentWarning') as TCastleUserInterface;
  ButtonQuitGame := DesignedComponent('ButtonQuitGame') as TCastleButton;
  ButtonQuitGame.OnClick := @ClickQuit;
  ButtonQuitGame.Exists := not ApplicationProperties.TouchDevice;
  ButtonStartGame := DesignedComponent('ButtonStartGame') as TCastleButton;
  ButtonStartGame.OnClick := @ClickContentWarningOk;

  Options := DesignedComponent('Options') as TCastleUserInterface;
  ButtonBackToMainMenu := DesignedComponent('ButtonBackToMainMenu') as TCastleButton;
  ButtonBackToMainMenu.OnClick := @DoMainMenu;
  ButtonToggleShakers := DesignedComponent('ButtonToggleShakers') as TCastleButton;
  ButtonToggleShakers.OnClick := @ClickToggleShakers;
  ButtonFullscreen := DesignedComponent('ButtonFullscreen') as TCastleButton;
  ButtonFullscreen.OnClick := @ClickFullscreen;
  ButtonCensored := DesignedComponent('ButtonCensored') as TCastleButton;
  ButtonCensored.OnClick := @ClickCensored;
  ButtonMasterVolume := DesignedComponent('ButtonMasterVolume') as TCastleButton;
  ButtonMasterVolume.OnClick := @ClickMasterVolume;
  ButtonMusicVolume := DesignedComponent('ButtonMusicVolume') as TCastleButton;
  ButtonMusicVolume.OnClick := @ClickMusicVolume;
  ButtonMapSize := DesignedComponent('ButtonMapSize') as TCastleButton;
  ButtonMapSize.OnClick := @ClickMapSize;
  ButtonTileset := DesignedComponent('ButtonTileset') as TCastleButton;
  ButtonTileset.OnClick := @ClickTileset;
  ButtonTileset.ClearControls;
  TilesetImage := TTileImage.Create(ButtonTileset);
  TilesetImage.Texture := Map.Texture;
  TilesetImage.Anchor(hpMiddle, hpMiddle, 0);
  TilesetImage.Anchor(vpMiddle, vpMiddle, 0);
  ButtonTileset.InsertFront(TilesetImage);
  ButtonHardcoreMode := DesignedComponent('ButtonHardcoreMode') as TCastleButton;
  ButtonHardcoreMode.OnClick := @ClickHardcoreMode;

  ButtonChangeCharacter := DesignedComponent('ButtonChangeCharacter') as TCastleButton;
  ButtonChangeCharacter.OnClick := @ClickChangeCharacter;
  LabelCharacterName := DesignedComponent('LabelCharacterName') as TCastleLabel;
  LabelCharacterScore := DesignedComponent('LabelCharacterScore') as TCastleLabel;
  ImageCharacterLocked := DesignedComponent('ImageCharacterLocked') as TCastleUserInterface;
  ButtonChangeCharacterSkin := DesignedComponent('ButtonChangeCharacterSkin') as TCastleButton;
  ButtonChangeCharacterSkin.OnClick := @ClickChangeSkin;
  LabelCharacterSkin := DesignedComponent('LabelCharacterSkin') as TCastleLabel;
  ImageSkinLocked := DesignedComponent('ImageSkinLocked') as TCastleUserInterface;

  ButtonResetProgress := DesignedComponent('ButtonResetProgress') as TCastleButton;
  ButtonResetProgress.OnClick := @ClickResetProgress;
  ConfirmResetProgress := DesignedComponent('ConfirmResetProgress') as TCastleUserInterface;
  ButtonResetProgressYes := DesignedComponent('ButtonResetProgressYes') as TCastleButton;
  ButtonResetProgressYes.OnClick := @ClickConfirmResetProgress;
  ButtonResetProgressNo := DesignedComponent('ButtonResetProgressNo') as TCastleButton;
  ButtonResetProgressNo.OnClick := @ClickOptions;

  Story := DesignedComponent('Story') as TCastleUserInterface;
  StoryLabel := DesignedComponent('StoryLabel') as TCastleLabel;
  ButtonLetsDoIt := DesignedComponent('ButtonLetsDoIt') as TCastleButton;
  ButtonLetsDoIt.OnClick := @StartGame;

  LastMusic := '';

  GameField := DesignedComponent('GameField') as TCastleUiShaker;
  GameField.InsertFront(Map);
  ScoreBox := DesignedComponent('ScoreBox') as TCastleUserInterface;
  ScoreLabel := DesignedComponent('ScoreLabel') as TCastleLabel;
  HighscoreCurrentImage := DesignedComponent('HighscoreCurrentImage') as TCastleUserInterface;
  HighscoreBox := DesignedComponent('HighscoreBox') as TCastleUserInterface;
  HighscoreLabel := DesignedComponent('HighscoreLabel') as TCastleLabel;
  HighscoreBeforeImage := DesignedComponent('HighscoreBeforeImage') as TCastleUserInterface;
  GameOverGroup := DesignedComponent('GameOverGroup') as TCastleUserInterface;
  GameOverText := DesignedComponent('GameOverText') as TCastleUserInterface;
  VictoryVerticalGroup := DesignedComponent('VictoryVerticalGroup') as TCastleUserInterface;
  FinalHighscoreImage := DesignedComponent('FinalHighscoreImage') as TCastleUserInterface;
  FinalScoreLabel := DesignedComponent('FinalScoreLabel') as TCastleLabel;
  HighscoreGroup := DesignedComponent('HighscoreGroup') as TCastleUserInterface;
  LabelNewCharacterUnlocked := DesignedComponent('LabelNewCharacterUnlocked') as TCastleLabel;
  LabelNewSkinUnlocked := DesignedComponent('LabelNewSkinUnlocked') as TCastleLabel;
  LabelUnlockHint := DesignedComponent('LabelUnlockHint') as TCastleLabel;
  ButtonAgain := DesignedComponent('ButtonAgain') as TCastleButton;
  ButtonAgain.OnClick := @StartGame;
  ButtonMenu := DesignedComponent('ButtonMenu') as TCastleButton;
  ButtonMenu.OnClick := @DoMainMenu;
  ButtonVictory := DesignedComponent('ButtonVictory') as TCastleButton;
  ButtonVictory.OnClick := @ClickWinGame;

  ManaBox := DesignedComponent('ManaBox') as TCastleImageControl;
  ManaBar := DesignedComponent('ManaBar') as TCastleImageControl;
  ManaBarLocked := DesignedComponent('ManaBarLocked') as TCastleImageControl;
  ManaDashCircleLeft := DesignedComponent('ManaDashCircleLeft') as TCastleImageControl;
  ManaDashCircleRight := DesignedComponent('ManaDashCircleRight') as TCastleImageControl;
  ManaChargeCircleLeft := DesignedComponent('ManaChargeCircleLeft') as TCastleImageControl;
  ManaChargeCircleRight := DesignedComponent('ManaChargeCircleRight') as TCastleImageControl;
  LavaBox := DesignedComponent('LavaBox') as TCastleImageControl;
  LavaBar := DesignedComponent('LavaBar') as TCastleImageControl;
  LavaBarLocked := DesignedComponent('LavaBarLocked') as TCastleImageControl;
  LavaDashCircleLeft := DesignedComponent('LavaDashCircleLeft') as TCastleImageControl;
  LavaDashCircleRight := DesignedComponent('LavaDashCircleRight') as TCastleImageControl;
  LavaChargeCircleLeft := DesignedComponent('LavaChargeCircleLeft') as TCastleImageControl;
  LavaChargeCircleRight := DesignedComponent('LavaChargeCircleRight') as TCastleImageControl;

  PauseMenu := DesignedComponent('PauseMenu') as TCastleUserInterface;
  ButtonPause := DesignedComponent('ButtonPause') as TCastleButton;
  ButtonPause.OnClick := @ClickPause;
  ButtonPauseUnpause := DesignedComponent('ButtonPauseUnpause') as TCastleButton;
  ButtonPauseUnpause.OnClick := @ClickPause;
  ButtonPauseRestart := DesignedComponent('ButtonPauseRestart') as TCastleButton;
  ButtonPauseRestart.OnClick := @StartGame;
  ButtonPauseToMainMenu := DesignedComponent('ButtonPauseToMainMenu') as TCastleButton;
  ButtonPauseToMainMenu.OnClick := @DoMainMenu;

  CharShaker := DesignedComponent('CharShaker') as TCastleUiShaker;
  CharBase := DesignedComponent('CharBase') as TCastleShiftedImageControl;
  CharCensored := DesignedComponent('CharCensored') as TCastleShiftedImageControl;
  for I := 0 to MaxCharHp do
    CharHitpoints[I] := DesignedComponent('CharHitpoints' + I.ToString) as TCastleShiftedImageControl;
  CharOverlay := DesignedComponent('CharOverlay') as TCastleShiftedImageControl;

  LabelVersion := DesignedComponent('LabelVersion') as TCastleLabel;
  LabelVersion.Caption := 'Version' + NL + ApplicationProperties.Version;

  TutorialBottom := DesignedComponent('TutorialBottom') as TCastleUserInterface;
  (DesignedComponent('TutorialBottomGraphics') as TCastleUserInterface).ClearControls;
  TutorialImage := TTileImage.Create(TutorialBottom);
  TutorialImage.Texture := Map.Texture;
  TutorialImage.Anchor(hpMiddle, hpMiddle, 0);
  TutorialImage.Anchor(vpMiddle, vpMiddle, 0);
  (DesignedComponent('TutorialBottomGraphics') as TCastleUserInterface).InsertFront(TutorialImage);
  TutorialBottomHeaderLabel := DesignedComponent('TutorialBottomHeaderLabel') as TCastleLabel;
  TutorialBottomTextLabel := DesignedComponent('TutorialBottomTextLabel') as TCastleLabel;
  TutorialTop := DesignedComponent('TutorialTop') as TCastleUserInterface;
  TutorialTopHeaderLabel := DesignedComponent('TutorialTopHeaderLabel') as TCastleLabel;
  TutorialTopTextLabel := DesignedComponent('TutorialTopTextLabel') as TCastleLabel;

  DoMainMenu(nil); // prepare UI for MainMenu, even if we show content warning
  if UserConfig.GetValue('ask_content_warning', true) then
  begin
    MainMenu.Exists := false;
    LabelVersion.Exists := false;
    ContentWarning.Exists := true;
    ButtonCheat.Exists := false;
  end;
end;

procedure TStateMain.DoMainMenu(Sender: TObject);
begin
  SelectedCharacter := CurrentCharacter;
  MainMenu.Exists := true;
  ContentWarning.Exists := false;
  CreditsMenu.Exists := false;
  Options.Exists := false;
  ScoreBox.Exists := false;
  HighscoreBox.Exists := false;
  GameOverGroup.Exists := false;
  GameOverText.Exists := true;
  VictoryVerticalGroup.Exists := false;
  FinalHighscoreImage.Exists := false;
  ManaBox.Exists := false;
  LavaBox.Exists := false;
  GameField.Exists := false;
  Story.Exists := false;
  LabelVersion.Exists := true;
  PauseMenu.Exists := false;
  ButtonPause.Exists := false;
  ConfirmResetProgress.Exists := false;
  TutorialBottom.Exists := false;
  WinGame.Exists := false;
  {$ifdef TouchController}
  TouchControl.Exists := false;
  {$endif}
  UpdateMapSizeTitle;
  SoundEngine.LoopingChannel[0].Sound := SoundEngine.SoundFromName('main_menu');
  if Sender <> nil then
    SoundEngine.Play(SoundEngine.SoundFromName('button'));
  SelectedSkin := CurrentSkin;
  SelectedCharacter := CurrentCharacter;
  ButtonCheat.Exists := true;
  SetCurrentCharacter;
  UpdateCharClothes;
end;

procedure TStateMain.ClickOptions(Sender: TObject);
begin
  if Sender <> nil then
    SoundEngine.Play(SoundEngine.SoundFromName('button'));
  MainMenu.Exists := false; // we guarantee we come here from MainMenu, no need to change everything else
  ButtonCheat.Exists := false;
  Options.Exists := true;
  ConfirmResetProgress.Exists := false;
  UpdateOptionsCaptions;
  ScrollViewOptions.Scroll := 0;
end;

procedure TStateMain.ClickResetProgress(Sender: TObject);
begin
  SoundEngine.Play(SoundEngine.SoundFromName('warning'));
  Options.Exists := false;
  ConfirmResetProgress.Exists := true;
end;

procedure TStateMain.ClickConfirmResetProgress(Sender: TObject);
begin
  SoundEngine.Play(SoundEngine.SoundFromName('warning'));
  UserConfig.Clear;
  ConfirmResetProgress.Exists := false;
  {$ifdef TouchController}
  Self.RemoveControl(TouchControl);
  FreeAndNil(TouchControl);
  {$endif}
  GameField.RemoveControl(Map);
  FreeAndNil(Map);
  StateMain.Stop;
  StateMain.Start;
end;

procedure TStateMain.ClickWinGame(Sender: TObject);
begin
  SoundEngine.Play(SoundEngine.SoundFromName('portal'));
  DoMainMenu(nil); // to properly reset all the game field to false
  SoundEngine.LoopingChannel[0].Sound := SoundEngine.SoundFromName('win_game'); //overwrite the menu track immediately
  MainMenu.Exists := false; // and hide the actual menu
  ButtonCheat.Exists := false;
  WinGame.Exists := true;
  WinGameScrollView.Scroll := 0;
end;

procedure TStateMain.ClickCredits(Sender: TObject);
begin
  SoundEngine.Play(SoundEngine.SoundFromName('button'));
  MainMenu.Exists := false; // we guarantee we come here from MainMenu, no need to change everything else
  ButtonCheat.Exists := false;
  CreditsMenu.Exists := true;
  ScrollViewCredits.Scroll := 0;
end;

procedure TStateMain.ClickQuit(Sender: TObject);
begin
  SoundEngine.Play(SoundEngine.SoundFromName('button'));
  Application.MainWindow.Close;
end;

procedure TStateMain.ClickContentWarningOk(Sender: TObject);
begin
  SoundEngine.Play(SoundEngine.SoundFromName('button'));
  UserConfig.SetValue('ask_content_warning', not (DesignedComponent('CheckboxDontAskAgain') as TCastleCheckbox).Checked);
  UserConfig.Save;
  DoMainMenu(nil);
end;

procedure TStateMain.ClickMapSize(Sender: TObject);
begin
  SoundEngine.Play(SoundEngine.SoundFromName('button'));
  Map.GetMapSize; // no need to but won't hurt
  case Map.SizeX of
    30: UserConfig.SetValue('map_size', 22);
    22: UserConfig.SetValue('map_size', 19);
    19: UserConfig.SetValue('map_size', 15);
    15: UserConfig.SetValue('map_size', 13);
    13: UserConfig.SetValue('map_size', 30); //44
    else UserConfig.SetValue('map_size', 15);
  end;
  UserConfig.Save;
  UpdateMapSizeTitle; // will also call GetMapSize
end;

procedure TStateMain.ClickTileset(Sender: TObject);
var
  PreferredMapKind: Integer;
begin
  SoundEngine.Play(SoundEngine.SoundFromName('button'));
  PreferredMapKind := UserConfig.GetValue('preferred_map_kind', -1);
  Inc(PreferredMapKind);
  if PreferredMapKind >= Map.MapTilesets then
    PreferredMapKind := -1;
  UserConfig.SetValue('preferred_map_kind', PreferredMapKind);
  UpdateOptionsCaptions;
end;

procedure TStateMain.ClickHardcoreMode(Sender: TObject);
var
  HardcoreMode: Integer;
begin
  SoundEngine.Play(SoundEngine.SoundFromName('button'));
  HardcoreMode := UserConfig.GetValue('hardcore_mode', 0);
  case HardcoreMode of
    0: HardcoreMode := 1;
    1: HardcoreMode := 2;
    2: HardcoreMode := 4;
    4: HardcoreMode := 8;
    8: HardcoreMode := 0;
    else
      HardcoreMode := 0;
  end;
  UserConfig.SetValue('hardcore_mode', HardcoreMode);
  UpdateOptionsCaptions;
end;

procedure TStateMain.ClickShowStory(Sender: TObject);
begin
  SoundEngine.Play(SoundEngine.SoundFromName('portal'));
  MainMenu.Exists := false; // we come to story only from Main Menu
  ButtonCheat.Exists := false;
  Story.Exists := true;
  StoryLabel.Caption := CharactersInfo[CurrentCharacter].Story + NL + NL + CharactersInfo[CurrentCharacter].CharacterSummary;
  ScrollViewStory.Scroll := 0;
end;

procedure TStateMain.UpdateMapSizeTitle;
begin
  Map.GetMapSize;
  ButtonMapSize.Caption := 'Map Size: ' + Map.SizeX.ToString + ' x ' + Map.SizeY.ToString;
end;

procedure TStateMain.ClickCheat(Sender: TObject);
begin
  if CheatTimer.ElapsedTime < 1 then
  begin
    Inc(CheatCount);
    if CheatCount >= 2 then
    begin
      // Unlock new character or skin
      if (CurrentCharacter = UserConfig.GetValue('max_unlocked_character', 0)) and (CurrentCharacter < CharactersInfo.Count - 1) then
      begin
        SoundEngine.Play(SoundEngine.SoundFromName('portal'));
        UserConfig.SetValue('max_unlocked_character', CurrentCharacter + 1);
        DoMainMenu(nil);
      end else
      if (UserConfig.GetValue('max_unlocked_skin_' + CurrentCharacter.ToString, 0) < CharactersInfo[CurrentCharacter].Skins.Count - 1) then
      begin
        SoundEngine.Play(SoundEngine.SoundFromName('portal'));
        UserConfig.SetValue('max_unlocked_skin_' + CurrentCharacter.ToString, UserConfig.GetValue('max_unlocked_skin_' + CurrentCharacter.ToString, 0) + 1);
        DoMainMenu(nil);
      end;
      CheatTimer := Timer;
      CheatCount := 0;
    end
  end else
  begin
    CheatTimer := Timer;
    CheatCount := 0;
  end;
end;

procedure TStateMain.QueueTutorial(const TutorialType: TTutorialType; const TutorialEntry: Integer);
var
  Rec: TTutorialRecord;
  IdString: String;
begin
  IdString := TutorialTypeToString(TutorialType) + TutorialEntry.ToString;
  if UserConfig.GetValue(IdString, true) then
  begin
    Rec.TutorialType := TutorialType;
    Rec.Entry := TutorialEntry;
    TutorialQueue.Add(Rec);
    UserConfig.SetValue(IdString, false);
  end;
end;

procedure TStateMain.ClickToggleShakers(Sender: TObject);
begin
  SoundEngine.Play(SoundEngine.SoundFromName('button'));
  UserConfig.SetValue('shaker', not UserConfig.GetValue('shaker', true));
  UserConfig.Save;
  UpdateOptionsCaptions;
end;

procedure TStateMain.ClickFullscreen(Sender: TObject);
begin
  SoundEngine.Play(SoundEngine.SoundFromName('button'));
  {$ifdef Android}
  UserConfig.SetValue('vibration', not UserConfig.GetValue('vibration', true));
  {$else}
  UserConfig.SetValue('fullscreen', not UserConfig.GetValue('fullscreen', true));
  Application.MainWindow.FullScreen := UserConfig.GetValue('fullscreen', true);
  {$endif}
  UserConfig.Save;
  UpdateOptionsCaptions;
end;

procedure TStateMain.ClickCensored(Sender: TObject);
begin
  SoundEngine.Play(SoundEngine.SoundFromName('button'));
  UserConfig.SetValue('censored_mode', not UserConfig.GetValue('censored_mode', false));
  UserConfig.Save;
  UpdateOptionsCaptions;
end;

procedure TStateMain.ClickMasterVolume(Sender: TObject);
var
  NewVolume: Single;
begin
  SoundEngine.Play(SoundEngine.SoundFromName('button'));
  NewVolume := 1.0;
  case Round(UserConfig.GetFloat('master_volume', 0.5) * 4) of
    0: NewVolume := 1.0;
    1: NewVolume := 0.0;
    2: NewVolume := 0.25;
    3: NewVolume := 0.5;
    4: NewVolume := 0.75;
  end;
  UserConfig.SetFloat('master_volume', NewVolume);
  UserConfig.Save;
  SoundEngine.Volume := UserConfig.GetFloat('master_volume', 0.5);
  UpdateOptionsCaptions;
end;

procedure TStateMain.ClickMusicVolume(Sender: TObject);
var
  NewVolume: Single;
begin
  SoundEngine.Play(SoundEngine.SoundFromName('button'));
  NewVolume := 1.0;
  case Round(UserConfig.GetFloat('music_volume', 1.0) * 4) of
    0: NewVolume := 1.0;
    1: NewVolume := 0.0;
    2: NewVolume := 0.25;
    3: NewVolume := 0.5;
    4: NewVolume := 0.75;
  end;
  UserConfig.SetFloat('music_volume', NewVolume);
  UserConfig.Save;
  SoundEngine.LoopingChannel[0].Volume := UserConfig.GetFloat('music_volume', 1.0);
  UpdateOptionsCaptions;
end;

procedure TStateMain.UpdateOptionsCaptions;
begin
  if UserConfig.GetValue('censored_mode', false) then
    ButtonCensored.Caption := 'Censored: ON'
  else
    ButtonCensored.Caption := 'Censored: OFF';
  {$ifdef Android}
  if UserConfig.GetValue('vibration', true) then
    ButtonFullscreen.Caption := 'Vibration: ON'
  else
    ButtonFullscreen.Caption := 'Vibration: OFF';
  {$else}
  if UserConfig.GetValue('fullscreen', true) then
    ButtonFullscreen.Caption := 'Fullscreen: ON'
  else
    ButtonFullscreen.Caption := 'Fullscreen: OFF';
  {$endif}
  if UserConfig.GetValue('shaker', true) then
    ButtonToggleShakers.Caption := 'Screen Shake: ON'
  else
    ButtonToggleShakers.Caption := 'Screen Shake: OFF';
  ButtonMasterVolume.Caption := 'Master Volume: ' + Round(100 * UserConfig.GetFloat('master_volume', 0.5)).ToString + '%';
  ButtonMusicVolume.Caption := 'Music Volume: ' + Round(100 * UserConfig.GetFloat('music_volume', 1.0)).ToString + '%';
  if UserConfig.GetValue('preferred_map_kind', -1) < 0 then
  begin
    ButtonTileset.Caption := '?';
    TilesetImage.Exists := false;
  end else
  begin
    ButtonTileset.Caption := '';
    TilesetImage.SetImageRect(0, 31 - UserConfig.GetValue('preferred_map_kind', -1));
    TilesetImage.Exists := true;
  end;

  if UserConfig.GetValue('hardcore_mode', 0) = 0 then
    ButtonHardcoreMode.Caption := 'Hardcore Mode: OFF'
  else
    ButtonHardcoreMode.Caption := 'Hardcore Mode: ' + UserConfig.GetValue('hardcore_mode', 0).ToString + 'x';
end;

procedure TStateMain.StartGame(Sender: TObject);
var
  NewMusic: String;
  P: Integer;
begin
  SoundEngine.Play(SoundEngine.SoundFromName('newgame'));
  MainMenu.Exists := false;
  ButtonCheat.Exists := false;

  CharShaker.StopShake;
  CharCensored.Exists := not UserConfig.GetValue('censored_mode', false);
  ScoreBox.Exists := true;
  ScoreLabel.Caption := '0';
  HighScore := UserConfig.GetValue('high_score' + CurrentCharacter.ToString, 0);
  HighscoreBox.Exists := Highscore > 0;
  HighscoreLabel.Caption := Highscore.ToString;
  HighscoreBeforeImage.Exists := Highscore > 0;
  HighscoreCurrentImage.Exists := false;
  LabelNewCharacterUnlocked.Exists := false;
  LabelNewSkinUnlocked.Exists := false;
  LabelUnlockHint.Exists := false;
  LabelVersion.Exists := false;
  Story.Exists := false;
  GameField.Exists := true;
  GameField.StopShake;
  PauseMenu.Exists := false;
  ButtonPause.Exists := true;
  TutorialBottom.Exists := false;
  {$ifdef TouchController}
  TouchControl.Exists := true;
  TouchControl.ReleaseMe; //make sure it doesn't have "memory" of the last input
  {$endif}

  GameOverGroup.Exists := false;
  GameOverText.Exists := true;
  VictoryVerticalGroup.Exists := false;
  FinalHighscoreImage.Exists := false;
  FinalScoreLabel.Caption := '0';

  Mana := CharactersInfo[CurrentCharacter].MaxMana; // no special treatment of skincaster here
  LastNoManaSound := Timer;
  Recharge := CharactersInfo[CurrentCharacter].RechargeSpeed; // no special treatment of skincaster here
  if CharactersInfo[CurrentCharacter].IsLava then
  begin
    ManaBox.Exists := false;
    LavaBox.Exists := true;
    CurrentBar := LavaBar;
    CurrentBarLocked := LavaBarLocked;
    CurrentDashCircleLeft := LavaDashCircleLeft;
    CurrentDashCircleRight := LavaDashCircleRight;
    CurrentChargeCircleLeft := LavaChargeCircleLeft;
    CurrentChargeCircleRight := LavaChargeCircleRight;
  end else
  begin
    ManaBox.Exists := true;
    LavaBox.Exists := false;
    CurrentBar := ManaBar;
    CurrentBarLocked := ManaBarLocked;
    CurrentDashCircleLeft := ManaDashCircleLeft;
    CurrentDashCircleRight := ManaDashCircleRight;
    CurrentChargeCircleLeft := ManaChargeCircleLeft;
    CurrentChargeCircleRight := ManaChargeCircleRight;
  end;
  CurrentDashCircleLeft.Exists := CharactersInfo[CurrentCharacter].Dash > 0;
  CurrentDashCircleRight.Exists := CurrentDashCircleLeft.Exists;
  CurrentChargeCircleLeft.Exists := (CharactersInfo[CurrentCharacter].RechargeSpeed > 0.5) or CharactersInfo[CurrentCharacter].IsSkincaster;
  CurrentChargeCircleRight.Exists := CurrentChargeCircleLeft.Exists;

  TutorialTimer := -1;
  if CharactersInfo[CurrentCharacter].Dash > 0 then
    QueueTutorial(ttPlayerDash, -1);
  if CharactersInfo[CurrentCharacter].DashThroughWalls then
    QueueTutorial(ttPlayerDashThroughWalls, -1);
  if CharactersInfo[CurrentCharacter].RechargeSpeed > 0.5 then
    QueueTutorial(ttPlayerSlowRecharge, -1);

  HardcoreModeCache := UserConfig.GetValue('hardcore_mode', 0) + 1;

  MonsterSpawnTimer := 0;
  MonstersToSpawn := 0;
  SetLength(PowerupsSpawnTimer, PowerupsInfo.Count);
  if HardcoreModeCache = 1 then
    for P := 0 to Pred(Length(PowerupsSpawnTimer)) do
      PowerupsSpawnTimer[P] := -CharactersInfo[CurrentCharacter].PowerupsSpawnInitialDelay[P]
  else
    for P := 0 to Pred(Length(PowerupsSpawnTimer)) do
      PowerupsSpawnTimer[P] := 0;
  SetLength(PowerupsToSpawn, PowerupsInfo.Count);
  for P := 0 to Pred(Length(PowerupsToSpawn)) do
    PowerupsToSpawn[P] := CharactersInfo[CurrentCharacter].PowerupsSpawnInitialQuantity[P];
  Map.Player.Shield := 0;
  Map.Player.SpeedBoost := 0;

  Map.GetMapSize;
  GameField.Width := Single(Map.SizeX)/ Single(Map.SizeY) * GameField.Height; // Height always stays the same
  Map.Generate;
  HP := CharactersInfo[CurrentCharacter].MaxHp;
  Score := 0;
  TotalTime := 0;
  UpdateCharClothes;
  GameOn := true;
  repeat
    if CharactersInfo[CurrentCharacter].IsLava then
      NewMusic := 'lava' + Rnd.Random(3).ToString
    else
      NewMusic := 'game' + Rnd.Random(3).ToString;
  until NewMusic <> LastMusic;
  LastMusic := NewMusic;
  SoundEngine.LoopingChannel[0].Sound := SoundEngine.SoundFromName(NewMusic);
end;

procedure TStateMain.HitPlayer;

  procedure StripExplosion;
  var
    I: Integer;
    EX, EY: Single;
    Bullet: TBullet;
  begin
    if CharactersInfo[CurrentCharacter].StripExplosion > 0 then
    begin
      if BulletsInfo[CharactersInfo[CurrentCharacter].StripBulletKind].ExplosionInteractionSize > 0 then
      begin
        for I := 0 to Pred(CharactersInfo[CurrentCharacter].StripExplosion) do
        begin
          repeat
            repeat
              EX := CharactersInfo[CurrentCharacter].StripExplosionRadius * 2 * (Rnd.Random - 0.5);
              EY := CharactersInfo[CurrentCharacter].StripExplosionRadius * 2 * (Rnd.Random - 0.5);
            until Sqr(EX) + Sqr(EY) <= CharactersInfo[CurrentCharacter].StripExplosionRadius;
            EX := Map.Player.X + EX;
            EY := Map.Player.Y + EY;
          until (EX >= 0) and (EY >= 0) and (EX <= Map.SizeX) and (EY <= Map.SizeY);
          Bullet := TBullet.Create;
          Bullet.Kind := CharactersInfo[CurrentCharacter].StripBulletKind;
          Bullet.AnimationTime := 0;
          Bullet.IsAlive := false;
          Bullet.X := EX;
          Bullet.Y := EY;
          Bullet.DX := 1;
          Bullet.DY := 0;
          Map.Bullets.Add(Bullet);
        end;
      end else
        raise Exception.Create('Character has strip explosion, but strip bullet kind doesn''t have ExplosionInteractionSize');
      if (BulletsInfo[CharactersInfo[CurrentCharacter].StripBulletKind].Shake > 0) and UserConfig.GetValue('shaker', true) then
        GameField.Shake(BulletsInfo[CharactersInfo[CurrentCharacter].StripBulletKind].Shake, 0.5);
      //SoundEngine.Play(SoundEngine.SoundFromName('shoot'));
    end;
  end;

  procedure StripFreeze;
  var
    I: Integer;
  begin
    if CharactersInfo[CurrentCharacter].StripFreeze > 0 then
      for I := 0 to Pred(Map.Monsters.Count) do
      begin
        if Map.Monsters[I].Frozen < 0 then
          Map.Monsters[I].Frozen := 0;
        Map.Monsters[I].Frozen += CharactersInfo[CurrentCharacter].StripFreeze;
        QueueTutorial(ttMonsterFrozen, -1);
      end;
  end;

  procedure StripBonuses;

    //Warning: copy from Update
    function GetMaxMana: Single;
    begin
      Result := CharactersInfo[CurrentCharacter].MaxMana;
      if CharactersInfo[CurrentCharacter].IsSkincaster then
        Result += SkincasterManaBonus * (CharactersInfo[CurrentCharacter].MaxHp - Hp);
    end;

  begin
    Score += CharactersInfo[CurrentCharacter].StripReward;
    Recharge += CharactersInfo[CurrentCharacter].StripRecharge;
    Mana += CharactersInfo[CurrentCharacter].StripMana;
    if Mana >= GetMaxMana then
      Mana := GetMaxMana;
    if CharactersInfo[CurrentCharacter].StripShield > 0 then
    begin
      if Map.Player.Shield < 0 then
        Map.Player.Shield := 0;
      Map.Player.Shield += CharactersInfo[CurrentCharacter].StripShield;
    end;
    if CharactersInfo[CurrentCharacter].StripSpeedBoost > 0 then
    begin
      if Map.Player.SpeedBoost < 0 then
        Map.Player.SpeedBoost := 0;
      Map.Player.SpeedBoost += CharactersInfo[CurrentCharacter].StripSpeedBoost;
    end;
  end;

  procedure EndGame;
  var
    RewardCharacter, RewardSkins: Boolean;
    SkinsUnlocked: Integer;
  begin
    if Score > HighScore then
    begin
      SoundEngine.Play(SoundEngine.SoundFromName('highscore'));
      UserConfig.SetValue('high_score' + CurrentCharacter.ToString, Score);
    end else
      SoundEngine.Play(SoundEngine.SoundFromName('gameover'));

    {$ifdef ANDROID}
    if UserConfig.GetValue('vibration', true) then
      Vibrate(500);
    {$endif}
    if UserConfig.GetValue('shaker', true) then
      CharShaker.Shake(30, 0.7);

    // Handle "New Character Unlocked!" and "New Skin Unlocked!" conditions
    RewardCharacter := false;
    if (CurrentCharacter = UserConfig.GetValue('max_unlocked_character', 0)) and (CurrentCharacter < CharactersInfo.Count - 1) and
       (Score >= CharactersInfo[CurrentCharacter].ScoreToUnlockNextCharacter) then
    begin
      RewardCharacter := true;
      LabelNewCharacterUnlocked.Exists := true;
      LabelNewCharacterUnlocked.Caption := NL + CharactersInfo[CurrentCharacter + 1].Name + NL + 'Unlocked!';
      UserConfig.SetValue('max_unlocked_character', CurrentCharacter + 1);
    end;
    RewardSkins := false;
    if (UserConfig.GetValue('max_unlocked_skin_' + CurrentCharacter.ToString, 0) < CharactersInfo[CurrentCharacter].Skins.Count - 1) and
       (Score >= CharactersInfo[CurrentCharacter].ScoreToUnlockNextSkin) then
    begin
      RewardSkins := true;
      SkinsUnlocked := 0;
      LabelNewSkinUnlocked.Exists := true;
      while (UserConfig.GetValue('max_unlocked_skin_' + CurrentCharacter.ToString, 0) < CharactersInfo[CurrentCharacter].Skins.Count - 1) and
       (Score >= CharactersInfo[CurrentCharacter].ScoreToUnlockNextSkin) do
      begin
         Inc(SkinsUnlocked);
         UserConfig.SetValue('max_unlocked_skin_' + CurrentCharacter.ToString, UserConfig.GetValue('max_unlocked_skin_' + CurrentCharacter.ToString, 0) + 1);
      end;
      if SkinsUnlocked = 1 then
        LabelNewSkinUnlocked.Caption := 'NEW' + NL + 'SKIN' + NL + 'UNLOCKED'
      else
        LabelNewSkinUnlocked.Caption := SkinsUnlocked.ToString + ' NEW' + NL + 'SKINS' + NL + 'UNLOCKED'
    end;
    if RewardCharacter or RewardSkins then
      SoundEngine.LoopingChannel[0].Sound := SoundEngine.SoundFromName('victory');
    HighscoreGroup.Exists := not (RewardCharacter and RewardSkins); // if two labels are displayed, don't show high score

    // Set "Game Won" for the last characters
    if (CurrentCharacter = UserConfig.GetValue('max_unlocked_character', 0)) and (Score >= CharactersInfo[CurrentCharacter].ScoreToUnlockNextCharacter) then
      UserConfig.SetValue('game_won', true);

    // And finally save configuration, even if nothing changed - it's a rare event
    UserConfig.Save;

    // Set "Victory" or "Menu" button depending on if the game was won
    ButtonVictory.Exists := (CurrentCharacter = UserConfig.GetValue('max_unlocked_character', 0)) and UserConfig.GetValue('game_won', false);
    ButtonMenu.Exists := not ButtonVictory.Exists;

    // Handle hint with the "next goal"
    if (CurrentCharacter = UserConfig.GetValue('max_unlocked_character', 0)) and (CurrentCharacter < CharactersInfo.Count - 1) then
    begin
      LabelUnlockHint.Exists := true;
      LabelUnlockHint.Caption :=
        'Win again with score at least ' + CharactersInfo[CurrentCharacter].ScoreToUnlockNextCharacter.ToString + NL +
        'to unlock a new character!';
    end else
    if (CurrentCharacter = CharactersInfo.Count - 1) and (not UserConfig.GetValue('game_won', false)) then
    begin
      LabelUnlockHint.Exists := true;
      LabelUnlockHint.Caption :=
        'Win again with score at least ' + CharactersInfo[CurrentCharacter].ScoreToUnlockNextCharacter.ToString + NL +
        'to win the game!';
    end else
    if (UserConfig.GetValue('max_unlocked_skin_' + CurrentCharacter.ToString, 0) < CharactersInfo[CurrentCharacter].Skins.Count - 1) then
    begin
      LabelUnlockHint.Exists := true;
      LabelUnlockHint.Caption :=
        'Win again with score at least ' + CharactersInfo[CurrentCharacter].ScoreToUnlockNextSkin.ToString + NL +
        'to unlock a new skin for this character!';
    end;

    GameOverGroup.Exists := true;
    GameOverText.Exists := (CurrentCharacter < CharactersInfo.Count - 1) or (Score < CharactersInfo[CurrentCharacter].ScoreToUnlockNextCharacter);
    VictoryVerticalGroup.Exists := not GameOverText.Exists;
    ManaBox.Exists := false;
    LavaBox.Exists := false;
    ScoreBox.Exists := not HighscoreGroup.Exists;
    GameField.Exists := false;
    PauseMenu.Exists := false;
    ButtonPause.Exists := false;
    {$ifdef TouchController}
    TouchControl.Exists := false;
    {$endif}
    FinalHighscoreImage.Exists := Score > HighScore;
    if Score > HighScore then
      HighScore := Score;
    FinalScoreLabel.Caption := Score.ToString;
    GameOn := false;
  end;

begin
  Dec(HP);
  if HP > 0 then
  begin
    SoundEngine.Play(SoundEngine.SoundFromName('rip_long_' + Rnd.Random(3).ToString));
    {$ifdef ANDROID}
    if UserConfig.GetValue('vibration', true) then
      Vibrate(200);
    {$endif}
    if UserConfig.GetValue('shaker', true) then
      CharShaker.Shake(20, 0.3);
  end;
  UpdateCharClothes;
  StripExplosion;
  StripFreeze;
  StripBonuses;
  if HP = 0 then
    EndGame;
end;

procedure TStateMain.UpdateCharClothes;
var
  I: Integer;
begin
  for I := 0 to MaxCharHp do
    CharHitpoints[I].Exists := HP = I;
end;

procedure TStateMain.ClickChangeCharacter(Sender: TObject);
begin
  SoundEngine.Play(SoundEngine.SoundFromName('changechar'));
  Inc(SelectedCharacter);
  if (SelectedCharacter >= CharactersInfo.Count) then
    SelectedCharacter := 0;
  if SelectedCharacter <= UserConfig.GetValue('max_unlocked_character', 0) then
  begin
    CurrentCharacter := SelectedCharacter;
    UserConfig.SetValue('current_character', CurrentCharacter);
    UserConfig.Save;
  end;
  SelectedSkin := UserConfig.GetValue('current_skin_' + CurrentCharacter.ToString, 0);
  SetCurrentCharacter;
  UpdateCharClothes;
end;

procedure TStateMain.ClickChangeSkin(Sender: TObject);
begin
  SoundEngine.Play(SoundEngine.SoundFromName('changechar'));
  Inc(SelectedSkin);
  if (SelectedSkin >= CharactersInfo[CurrentCharacter].Skins.Count) then
    SelectedSkin := 0;
  if SelectedSkin <= UserConfig.GetValue('max_unlocked_skin_' + CurrentCharacter.ToString, 0) then
  begin
    CurrentSkin := SelectedSkin;
    UserConfig.SetValue('current_skin_' + CurrentCharacter.ToString, CurrentSkin);
    UserConfig.Save;
  end;
  SetCurrentCharacter;
  UpdateCharClothes;
end;

procedure TStateMain.SetCurrentCharacter;
var
  I: Integer;
begin
  CurrentSkin := UserConfig.GetValue('current_skin_' + CurrentCharacter.ToString, 0);
  CharBase.Url := CharactersInfo[CurrentCharacter].Skins[CurrentSkin].BaseImg;
  CharCensored.Url := CharactersInfo[CurrentCharacter].Skins[CurrentSkin].CensoredImg;
  for I := 0 to MaxCharHp do
    CharHitpoints[I].Url := CharactersInfo[CurrentCharacter].Skins[CurrentSkin].HpImg[I];
  CharOverlay.Url := CharactersInfo[CurrentCharacter].Skins[CurrentSkin].OverlayImg;

  //This concerns only selected character, not the current one
  LabelCharacterName.Caption := CharactersInfo[SelectedCharacter].Name;
  if SelectedCharacter > UserConfig.GetValue('max_unlocked_character', 0) then
    LabelCharacterScore.Caption := '[Locked]'
  else
  begin
    if (SelectedCharacter = UserConfig.GetValue('max_unlocked_character', 0)) and
       (SelectedCharacter < CharactersInfo.Count - 1) then
      LabelCharacterScore.Caption := 'Progress to next character: ' + UserConfig.GetValue('high_score' + SelectedCharacter.ToString, 0).ToString + '/' + CharactersInfo[SelectedCharacter].ScoreToUnlockNextCharacter.ToString
    else
    if (SelectedCharacter = CharactersInfo.Count - 1) and not UserConfig.GetValue('game_won', false) then
      LabelCharacterScore.Caption := 'Progress to victory: ' + UserConfig.GetValue('high_score' + SelectedCharacter.ToString, 0).ToString + '/' + CharactersInfo[SelectedCharacter].ScoreToUnlockNextCharacter.ToString
    else
    if UserConfig.GetValue('max_unlocked_skin_' + CurrentCharacter.ToString, 0) < CharactersInfo[CurrentCharacter].Skins.Count - 1 then
      LabelCharacterScore.Caption := 'Score: ' + UserConfig.GetValue('high_score' + SelectedCharacter.ToString, 0).ToString + '/' + CharactersInfo[SelectedCharacter].ScoreToUnlockNextSkin.ToString + ' (Skins to unlock: ' + (CharactersInfo[CurrentCharacter].Skins.Count - UserConfig.GetValue('max_unlocked_skin_' + CurrentCharacter.ToString, 0) - 1).ToString + ')'
    else
      LabelCharacterScore.Caption := 'High score for ' + CharactersInfo[SelectedCharacter].Name + ': ' + UserConfig.GetValue('high_score' + SelectedCharacter.ToString, 0).ToString;
  end;
  LabelCharacterSkin.Caption := CharactersInfo[SelectedCharacter].Skins[SelectedSkin].Name;

  if SelectedCharacter > UserConfig.GetValue('max_unlocked_character', 0) then
  begin
    ImageCharacterLocked.Exists := true;
    LabelCharacterName.FontScale := 0.8;
    ButtonChangeCharacterSkin.Exists := false;
  end else
  begin
    ImageCharacterLocked.Exists := false;
    LabelCharacterName.FontScale := 1.0;
    ButtonChangeCharacterSkin.Exists := true;
  end;

  if SelectedSkin > UserConfig.GetValue('max_unlocked_skin_' + CurrentCharacter.ToString, 0) then
  begin
    ImageSkinLocked.Exists := true;
    LabelCharacterSkin.FontScale := 0.8;
  end else
  begin
    ImageSkinLocked.Exists := false;
    LabelCharacterSkin.FontScale := 1.0;
  end;

  HP := CharactersInfo[CurrentCharacter].MenuHp;
end;

procedure TStateMain.ClickPause(Sender: TObject);
begin
  if ScoreBox.Exists then // a cheaty way to know if the game is currently running
  begin
    GameOn := not GameOn;
    PauseMenu.Exists := not GameOn;
    ButtonPause.Exists := GameOn;
    GameField.Exists := GameOn;
    GameField.StopShake;
  end;
end;

{$ifndef Android}
function TStateMain.Press(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited;
  if Result then
    Exit;
  if Event.IsKey(keyEscape) then
    ClickPause(nil);
  if Event.IsKey(keyF11) then
    ClickFullscreen(nil);
  if Event.IsKey(keyF5) then
  begin
    Container.SaveScreen('screenshot_foxy-misfortunes_' + IntToStr(Round(Now * 24 * 60 * 60 * 100)) + '.png');
    SoundEngine.Play(SoundEngine.SoundFromName('screenshot'));
  end;
end;
{$endif}

procedure TStateMain.Update(const SecondsPassed: Single; var HandleInput: Boolean);

  function GetMaxMana: Single;
  begin
    Result := CharactersInfo[CurrentCharacter].MaxMana;
    if CharactersInfo[CurrentCharacter].IsSkincaster then
      Result += SkincasterManaBonus * (CharactersInfo[CurrentCharacter].MaxHp - Hp);
  end;

  function GetRecharge: Single;
  begin
    Result := CharactersInfo[CurrentCharacter].RechargeSpeed;
    if CharactersInfo[CurrentCharacter].IsSkincaster then
      Result -= SkincasterRechargeBonus * (CharactersInfo[CurrentCharacter].MaxHp - Hp);
  end;

  function GetSpeed: Single;
  begin
    Result := CharactersInfo[CurrentCharacter].Speed;
    if CharactersInfo[CurrentCharacter].IsSkincaster then
      Result += SkincasterSpeedBonus * (CharactersInfo[CurrentCharacter].MaxHp - Hp);
  end;

  function GetBulletCost: Single;
  begin
    Result := CharactersInfo[CurrentCharacter].BulletCost;
    if CharactersInfo[CurrentCharacter].IsSkincaster then
      Result -= SkincasterBulletCostBonus * (CharactersInfo[CurrentCharacter].MaxHp - Hp);
  end;

  procedure PlayerManaRegeneration;
  var
    ManaLockResist: Single;
  begin
    if Map.Player.ManaLock <= 0 then
    begin
      Mana += SecondsPassed;
      if Mana > GetMaxMana then
        Mana := GetMaxMana;
      CurrentBar.Exists := true;
      CurrentBarLocked.Exists := false;
      CurrentBar.ClipLine := Vector3(-1, 0, Mana / GetMaxMana);
    end else
    begin
      ManaLockResist := CharactersInfo[CurrentCharacter].ManaLockResistance;
      if CharactersInfo[CurrentCharacter].IsSkincaster then
        ManaLockResist += SkincasterManaLockResistanceBonus * (CharactersInfo[CurrentCharacter].MaxHp - Hp);
      Map.Player.ManaLock -= SecondsPassed * ManaLockResist;
      CurrentBar.Exists := false;
      CurrentBarLocked.Exists := true;
      CurrentBarLocked.ClipLine := Vector3(-1, 0, Mana / GetMaxMana);
    end;
  end;

  procedure SetCliplines(const ChargeFrac: Single; const LeftCircle, RightCircle: TCastleImageControl);
  var
    F1, F2, C1, S1, C2, S2: Single;
  begin
    if ChargeFrac > 0.5 then
    begin
      F2 := 2 * (ChargeFrac - 0.5);
      if F2 > 1 then
        F2 := 1;
      F1 := 1;
      C1 := -1; //Cos(Pi)
      S1 := 0;  //Sin(Pi)
      C2 := Cos(Pi * F2);
      S2 := Sin(Pi * F2);
    end else
    begin
      F2 := 0;
      F1 := 2 * ChargeFrac;
      C1 := Cos(Pi * F1);
      S1 := Sin(Pi * F1);
      C2 := 1;
      S2 := 0;
    end;
    //It took me over an hour to figure out these formulae, DON'T TOUCH THEM unless you know what you are doing
    LeftCircle.ClipLine := Vector3(C1, S1, -C1 - S1 / 2);
    RightCircle.ClipLine := Vector3(-C2, -S2, S2 / 2);
  end;

  procedure PlayerRecharge;
  begin
    if Map.Player.IsFrozen > 0 then
      Recharge += SecondsPassed / 2
    else
      Recharge += SecondsPassed;
    SetCliplines(Recharge / GetRecharge, CurrentChargeCircleLeft, CurrentChargeCircleRight);
  end;

  procedure MovePlayer;
  var
    MoveX, MoveY: Single;
    DX, DY: Single;
    Speed: Single;
    Mag: Single;
    FreezeResist: Single;
  begin
    Map.Player.Shield -= SecondsPassed;
    Map.Player.SpeedBoost -= SecondsPassed;
    {$ifdef TouchController}
    MoveX := TouchControl.FLeverOffset.X;
    MoveY := TouchControl.FLeverOffset.Y;
    {$else}
    MoveX := 0;
    MoveY := 0;
    if Container.Pressed[keyW] or Container.Pressed[keyArrowUp] then
      MoveY += 1.0;
    if Container.Pressed[keyS] or Container.Pressed[keyArrowDown] then
      MoveY -= 1.0;
    if Container.Pressed[keyA] or Container.Pressed[keyArrowLeft] then
      MoveX -= 1.0;
    if Container.Pressed[keyD] or Container.Pressed[keyArrowRight] then
      MoveX += 1.0;
    {$endif}
    // normalize speed
    Mag := Sqrt(Sqr(MoveX) + Sqr(MoveY));
    if (Mag > 0.1) then
    begin
      MoveX := MoveX / Mag;
      MoveY := MoveY / Mag;
    end else
    begin
      if CharactersInfo[CurrentCharacter].Dash > 0 then
      begin
        Map.Player.Dash += SecondsPassed * CharactersInfo[CurrentCharacter].DashRecharge;
        if Map.Player.Dash > CharactersInfo[CurrentCharacter].Dash then
          Map.Player.Dash := CharactersInfo[CurrentCharacter].Dash;
        SetCliplines(Map.Player.Dash / CharactersInfo[CurrentCharacter].Dash, CurrentDashCircleLeft, CurrentDashCircleRight);
      end;
      Exit;
    end;

    if Map.Player.Dash > 0 then
      Speed := CharactersInfo[CurrentCharacter].DashSpeed * SecondsPassed
    else
      Speed := GetSpeed * SecondsPassed;
    if Map.Player.IsFrozen >= 0 then
    begin
      Speed := Speed / 2;
      FreezeResist := CharactersInfo[CurrentCharacter].FreezeResistance;
      Map.Player.IsFrozen -= SecondsPassed * FreezeResist;
    end;
    if Map.Player.SpeedBoost >= 0 then
      Speed *= 1.5;
    DX := Speed * MoveX;
    DY := Speed * MoveY;
    if not Map.CanMove(Map.Player.X + DX, Map.Player.Y + DY, CharactersInfo[CurrentCharacter].CollisionSize) then
    begin
      if Map.CanMove(Map.Player.X, Map.Player.Y + DY, CharactersInfo[CurrentCharacter].CollisionSize) then
        DX := 0
      else
      if Map.CanMove(Map.Player.X + DX, Map.Player.Y, CharactersInfo[CurrentCharacter].CollisionSize) then
        DY := 0
      else
      begin
        DX := 0;
        DY := 0;
      end;
    end{ else
      if not Map.CanMove(Map.Player.X, Map.Player.Y + DY, CharactersInfo[CurrentCharacter].CollisionSize) and not Map.CanMove(Map.Player.X + DX, Map.Player.Y, CharactersInfo[CurrentCharacter].CollisionSize) then //corners
      begin
        DX := 0;
        DY := 0;
      end};
    if (DX <> 0) or (DY <> 0) then
    begin
      //move is possible
      if Sqr(DX) + Sqr(DY) > Sqr(Speed / 4) then
      begin
        if Map.Player.Dash > 0 then
        begin
          Map.Player.Dash -= SecondsPassed;
          SetCliplines(Map.Player.Dash / CharactersInfo[CurrentCharacter].Dash, CurrentDashCircleLeft, CurrentDashCircleRight);
        end;
        // do not wast DashCharge if the Player isn't really moving
        Map.Player.DashCharge := 0;
      end;
      Map.Player.X += DX;
      Map.Player.Y += DY;
    end else
    if CharactersInfo[CurrentCharacter].DashThroughWalls then
    begin
      if Map.Player.Dash > Map.Player.DashCharge then
      begin
        Map.Player.DashCharge += SecondsPassed;
        //try dash through wall
        Speed := CharactersInfo[CurrentCharacter].DashSpeed * Map.Player.DashCharge;
        DX := Speed * MoveX;
        DY := Speed * MoveY;
        if Map.CanMove(Map.Player.X + DX, Map.Player.Y + DY, CharactersInfo[CurrentCharacter].CollisionSize) then
        begin
          Map.Player.X += DX;
          Map.Player.Y += DY;
          Map.Player.Dash -= Map.Player.DashCharge;
          SetCliplines(Map.Player.Dash / CharactersInfo[CurrentCharacter].Dash, CurrentDashCircleLeft, CurrentDashCircleRight);
          Map.Player.DashCharge := 0;
        end;
      end else
        Map.Player.DashCharge := 0;
    end;
  end;

  procedure PlayerAndBullets;
  var
    J: Integer;
  begin
    for J := 0 to Pred(Map.Bullets.Count) do
      if BulletsInfo[Map.Bullets[J].Kind].AffectsPlayer <> apNone then
        if Map.Bullets[J].IsAlive then
          if Sqr(Map.Bullets[J].X - Map.Player.X) + Sqr(Map.Bullets[J].Y - Map.Player.Y) < Sqr((BulletsInfo[Map.Bullets[J].Kind].InteractionSize + CharactersInfo[CurrentCharacter].InteractionSize) / 2) then
          begin
            case BulletsInfo[Map.Bullets[J].Kind].AffectsPlayer of
              apFreeze:
                begin
                  if Map.Player.IsFrozen < 0 then
                    Map.Player.IsFrozen := 0;
                  Map.Player.IsFrozen += 5;
                  QueueTutorial(ttPlayerFrozen, -1);
                  SoundEngine.Play(SoundEngine.SoundFromName('freezeshothit'));
                end;
              apManaDrain:
                begin
                  if Map.Player.ManaLock < 0 then
                    Map.Player.ManaLock := 0;
                  Map.Player.ManaLock += 5;
                  QueueTutorial(ttPlayerManaLock, -1);
                  SoundEngine.Play(SoundEngine.SoundFromName('manalockshothit'));
                end;
              else
                raise Exception.Create('Unexpected AffectPlayer type');
            end;
            Map.Bullets[J].IsAlive := false;
            Map.Bullets[J].AnimationTime := 0;
          end;
  end;

  procedure FireBullets;
  var
    Bullet: TBullet;
    Mag: Single;
    TouchPos: TVector2;

    function GetTouch: Boolean;
    {$ifdef TouchController}
    var
      T: Integer;
    {$endif}
    begin
      {$ifdef TouchController}
      if (Container.TouchesCount > 0) then
      begin
        for T := 0 to Pred(Container.TouchesCount) do
          if (Container.Touches[T].FingerIndex <> TouchControl.FingerDragging) and
            (Container.Touches[T].Position.X > GameField.RenderRect.Left - 100) and
            (Container.Touches[T].Position.X < GameField.RenderRect.Right + 40) then  // we need smaller margin due to pause button
          begin
            TouchPos := Container.Touches[T].Position;
            Exit(true);
          end;
      end;
      {$else}
      if (buttonLeft in Container.MousePressed) and
        (Container.MousePosition.X > GameField.RenderRect.Left - 100) and
        (Container.MousePosition.X < GameField.RenderRect.Right + 40) then  // we need smaller margin due to pause button
      begin
        TouchPos := Container.MousePosition;
        Exit(true);
      end;
      {$endif}
      Result := false;
    end;

  begin
    if (Recharge >= GetRecharge) and GetTouch then
    begin
      if Mana < GetBulletCost then
      begin
        if LastNoManaSound.ElapsedTime > 0.3 then
        begin
          SoundEngine.Play(SoundEngine.SoundFromName('nomana'));
          QueueTutorial(ttNoMana, -1);
          LastNoManaSound := Timer;
        end;
        Exit;
      end;
      Recharge := 0;
      Mana -= GetBulletCost;
      if CharactersInfo[CurrentCharacter].ShieldOnShot > 0 then
      begin
        if Map.Player.Shield < 0 then
          Map.Player.Shield := 0;
        Map.Player.Shield += CharactersInfo[CurrentCharacter].ShieldOnShot;
      end;
      if CharactersInfo[CurrentCharacter].SpeedOnShot > 0 then
      begin
        if Map.Player.SpeedBoost < 0 then
          Map.Player.SpeedBoost := 0;
        Map.Player.SpeedBoost += CharactersInfo[CurrentCharacter].SpeedOnShot;
      end;
      Bullet := TBullet.Create;
      Bullet.Kind := CharactersInfo[CurrentCharacter].Skins[CurrentSkin].BulletKind;
      Bullet.AnimationTime := 0;
      Bullet.IsAlive := true;
      Bullet.DX := Map.ScreenToMapX(TouchPos.X) - Map.Player.X;
      Bullet.DY := Map.ScreenToMapY(TouchPos.Y) - Map.Player.Y;
      Mag := Sqrt(Sqr(Bullet.DX) + Sqr(Bullet.DY));
      if Mag = 0 then
      begin
        Bullet.DX := 1;
        Mag := 1;
      end;
      Bullet.DX := Bullet.DX / Mag;
      Bullet.DY := Bullet.DY / Mag;
      Bullet.X := Map.Player.X + Bullet.DX * CharactersInfo[CurrentCharacter].InteractionSize / 2;
      Bullet.Y := Map.Player.Y + Bullet.DY * CharactersInfo[CurrentCharacter].InteractionSize / 2;
      Map.Bullets.Add(Bullet);
      if CharactersInfo[CurrentCharacter].IsLava then
        SoundEngine.Play(SoundEngine.SoundFromName('lavashoot'))
      else
        SoundEngine.Play(SoundEngine.SoundFromName('shoot'));
    end;
  end;

  procedure SpawnDecal(const Entity: TEntity; const Kind: Integer);
  var
    Decal: TDecal;
  begin
    if Kind < 0 then
      Exit;
    Decal := TDecal.Create;
    Decal.Kind := Kind;
    Decal.Timeout := 10;
    Decal.RandomSize := 0.9 + Rnd.Random * 0.2;
    Decal.X := Entity.X;
    Decal.Y := Entity.Y;
    Map.Decals.Add(Decal);
    QueueTutorial(ttDecal, Decal.Kind);
    case DecalsInfo[Decal.Kind].Effect of
      deFreeze: SoundEngine.Play(SoundEngine.SoundFromName('freezedecal'));
      deManaLock: SoundEngine.Play(SoundEngine.SoundFromName('manalockdecal'));
      deNone: ;
      else
        raise Exception.Create('Unexpected Decal effect');
    end;
  end;

  procedure UpdateBullets;
  var
    I, J, LockOnMonster: Integer;
    SqrDistToMonster, R, MonsterDX, MonsterDY: Single;
    NewX, NewY: Single;
    Speed: Single;
  begin
    I := 0;
    while I < Map.Bullets.Count do
    begin
      Map.Bullets[I].AnimationTime += SecondsPassed;
      if not Map.Bullets[I].IsAlive then
      begin
        if Map.Bullets[I].AnimationTime > DeathAnimationTime then
          Map.Bullets.Delete(I)
        else
          Inc(I);
        Continue;
      end;
      // Homing bullets
      if BulletsInfo[Map.Bullets[I].Kind].Homing > 0 then
      begin
        LockOnMonster := -1;
        SqrDistToMonster := 9999;
        for J := 0 to Pred(Map.Monsters.Count) do
        begin
          R := Sqr(Map.Monsters[J].X - Map.Bullets[I].X) + Sqr(Map.Monsters[J].Y - Map.Bullets[I].Y);
          if SqrDistToMonster > R then
          begin
            SqrDistToMonster := R;
            LockOnMonster := J;
          end;
        end;
        if LockOnMonster >= 0 then
        begin
          if SqrDistToMonster < 1 then
            SqrDistToMonster := 1;
          R := Sqrt(SqrDistToMonster);
          MonsterDX := (Map.Monsters[LockOnMonster].X - Map.Bullets[I].X) / R;
          MonsterDY := (Map.Monsters[LockOnMonster].Y - Map.Bullets[I].Y) / R;
          Map.Bullets[I].DX := (1 - BulletsInfo[Map.Bullets[I].Kind].Homing) * Map.Bullets[I].DX + BulletsInfo[Map.Bullets[I].Kind].Homing * MonsterDX;
          Map.Bullets[I].DY := (1 - BulletsInfo[Map.Bullets[I].Kind].Homing) * Map.Bullets[I].DY + BulletsInfo[Map.Bullets[I].Kind].Homing * MonsterDY;
          R := Sqrt(Sqr(Map.Bullets[I].DX) + Sqr(Map.Bullets[I].DY));
          Map.Bullets[I].DX := Map.Bullets[I].DX / R;
          Map.Bullets[I].DY := Map.Bullets[I].DY / R;
        end;
      end;
      Speed := BulletsInfo[Map.Bullets[I].Kind].Speed * SecondsPassed;
      NewX := Map.Bullets[I].X + Speed * Map.Bullets[I].DX;
      NewY := Map.Bullets[I].Y + Speed * Map.Bullets[I].DY;
      if Map.CanMove(NewX, NewY, BulletsInfo[Map.Bullets[I].Kind].CollisionSize) then
      begin
        Map.Bullets[I].X := NewX;
        Map.Bullets[I].Y := NewY;
      end else
      begin
        Map.Bullets[I].IsAlive := false;
        Map.Bullets[I].AnimationTime := 0;
        SpawnDecal(Map.Bullets[I], BulletsInfo[Map.Bullets[I].Kind].ExplosionDecal);
        if (BulletsInfo[Map.Bullets[I].Kind].Shake > 0) and UserConfig.GetValue('shaker', true) then
          GameField.Shake(BulletsInfo[Map.Bullets[I].Kind].Shake, 0.3);
        SoundEngine.Play(SoundEngine.SoundFromName(BulletsInfo[Map.Bullets[I].Kind].ImpactSound));
        Continue;
      end;
      Inc(I);
    end;
  end;

  procedure UpdateDecals;
  var
    I: Integer;

    procedure DecalsAndExplosions;
    var
      J: Integer;
    begin
      if DecalsInfo[Map.Decals[I].Kind].CanBeCleared then
        for J := 0 to Pred(Map.Bullets.Count) do
          if not Map.Bullets[J].IsAlive and
            BulletsInfo[Map.Bullets[J].Kind].ExplosionClearsDecals and
            (Sqr(Map.Decals[I].X - Map.Bullets[J].X) + Sqr(Map.Decals[I].Y - Map.Bullets[J].Y) < Sqr(BulletsInfo[Map.Bullets[J].Kind].ExplosionInteractionSize * Map.Bullets[J].AnimationTime / BulletDeathAnimationTime)) then
              Map.Decals[I].Timeout := -1;
    end;

  begin
    I := 0;
    while I < Map.Decals.Count do
    begin
      DecalsAndExplosions;
      Map.Decals[I].Timeout -= SecondsPassed;

      if Map.Decals[I].Timeout < 0 then
        Map.Decals.Delete(I)
      else
      if Sqr(Map.Player.X - Map.Decals[I].X) + Sqr(Map.Player.Y - Map.Decals[I].Y) <= Sqr(CharactersInfo[CurrentCharacter].InteractionSize / 2 + DecalsInfo[Map.Decals[I].Kind].InteractionSize / 2) then
      begin
        case DecalsInfo[Map.Decals[I].Kind].Effect of
          deFreeze:
            begin
              if Map.Player.IsFrozen < 0 then
                Map.Player.IsFrozen := 0;
              Map.Player.IsFrozen += DecalsInfo[Map.Decals[I].Kind].EffectStrength;
              Map.Decals.Delete(I);
              QueueTutorial(ttPlayerFrozen, -1);
              SoundEngine.Play(SoundEngine.SoundFromName('freezedecalhit'));
            end;
          deManaLock:
            begin
              if Map.Player.ManaLock < 0 then
                Map.Player.ManaLock := 0;
              Map.Player.ManaLock += DecalsInfo[Map.Decals[I].Kind].EffectStrength;
              Map.Decals.Delete(I);
              QueueTutorial(ttPlayerManaLock, -1);
              SoundEngine.Play(SoundEngine.SoundFromName('manalockdecalhit'));
            end;
          deNone:
            Inc(I);
          else
              raise Exception.Create('Unexpected Decal effect');
        end;
      end else
        Inc(I);
    end;
  end;

  procedure SpawnPowerups;
  var
    P: Integer;
    X, Y: Integer;
    Powerup: TPowerup;

    procedure TrySpawnPowerup;
    var
      I: Integer;
    begin
      Powerup := TPowerup.Create;
      Powerup.Kind := P;
      Powerup.X := X + 0.5;
      Powerup.Y := Y + 0.5;
      for I := 0 to Pred(Map.Powerups.Count) do
        if Sqr(Powerup.X - Map.Powerups[I].X) + Sqr(Powerup.Y - Map.Powerups[I].Y) < 1 then
        begin
          Powerup.Free;
          Exit;
        end;
      Map.Powerups.Add(Powerup);
      QueueTutorial(ttPowerup, Powerup.Kind);
      Dec(PowerupsToSpawn[P]);
    end;

  begin
    for P := 0 to Pred(PowerupsInfo.Count) do
    begin
      PowerupsSpawnTimer[P] += SecondsPassed;
      if (PowerupsSpawnTimer[P] > CharactersInfo[CurrentCharacter].PowerupsSpawnDelay[P] / Sqrt(Sqrt(HardcoreModeCache))) then
      begin
        PowerupsSpawnTimer[P] -= CharactersInfo[CurrentCharacter].PowerupsSpawnDelay[P] / Sqrt(Sqrt(HardcoreModeCache));
        Inc(PowerupsToSpawn[P]);
      end;
      if PowerupsToSpawn[P] = 0 then
        Continue;
      repeat
        X := Rnd.Random(Map.SizeX);
        Y := Rnd.Random(Map.SizeY);
      until Sqr(Map.Player.X - X) + Sqr(Map.Player.Y - Y) > Sqr(Map.SizeY / 3);
      if (Map.CanMove(X, Y, PowerupsInfo[P].InteractionSize)) then
        TrySpawnPowerup;
    end;
  end;

  procedure UpdatePowerups;
  var
    I: Integer;
  begin
    I := 0;
    while I < Map.Powerups.Count do
    begin
      if Sqr(Map.Player.X - Map.Powerups[I].X) + Sqr(Map.Player.Y - Map.Powerups[I].Y) < Sqr((CharactersInfo[CurrentCharacter].InteractionSize + PowerupsInfo[Map.Powerups[I].Kind].InteractionSize) / 2) then
      begin
        Score += PowerupsInfo[Map.Powerups[I].Kind].Reward;
        case PowerupsInfo[Map.Powerups[I].Kind].Effect of
          peNone: ; //do nothing
          peMana:
            begin
              Mana += PowerupsInfo[Map.Powerups[I].Kind].EffectStrength;
              if Mana > GetMaxMana then
                Mana := GetMaxMana;
            end;
          peShield:
            begin
              if Map.Player.Shield < 0 then
                Map.Player.Shield := 0;
              Map.Player.Shield += PowerupsInfo[Map.Powerups[I].Kind].EffectStrength;
              QueueTutorial(ttPlayerShield, -1);
            end;
          peSpeed:
            begin
              if Map.Player.SpeedBoost < 0 then
                Map.Player.SpeedBoost := 0;
              Map.Player.SpeedBoost += PowerupsInfo[Map.Powerups[I].Kind].EffectStrength;
            end;
          else
            raise Exception.Create('Unexpected powerup effect');
        end;
        SoundEngine.Play(SoundEngine.SoundFromName(PowerupsInfo[Map.Powerups[I].Kind].Sound));
        Map.Powerups.Delete(I);
        Continue;
      end;
      Inc(I);
    end;
  end;

  procedure SpawnMonsters;
  const
    { value in seconds (squared):
      0.4 * Difficulty curve = 50% error margin
      1 * Difficulty curve = 25%
      2 * Difficulty curve = 11%
      3 * Difficulty curve = 6%
      4 * Difficulty curve = 4%
      5 * Difficulty curve = 3%
      9 * Difficulty curve = 1% error margin
    }
    { value in seconds (linear):
      1 * Difficulty curve = 50% error margin
      2 * Difficulty curve = 33%
      3 * Difficulty curve = 25%
      4 * Difficulty curve = 20%
      9 * Difficulty curve = 10%
    }
    DifficultyCurve = 30;
    //{$ifdef Android}120{$else}60{$endif}; <---- this is a bad idea as it increases the game duration instead of making it more fun, better reduce the unlock threshold and cope with the fact that Android is twice as hard to play
  var
    I: Integer;
    X, Y: Integer;
    Monster: TMonster;
    SpawnDelay: Single;
  begin
    MonsterSpawnTimer += SecondsPassed;
    SpawnDelay := CharactersInfo[CurrentCharacter].MinMonsterSpawnDelay + (CharactersInfo[CurrentCharacter].MaxMonsterSpawnDelay - CharactersInfo[CurrentCharacter].MinMonsterSpawnDelay) * {Sqr}(DifficultyCurve / (TotalTime * HardcoreModeCache + DifficultyCurve)); // 10% reserve after 4.5 minutes
    if MonsterSpawnTimer > SpawnDelay then
    begin
      MonsterSpawnTimer -= SpawnDelay;
      Inc(MonstersToSpawn);
    end;
    if MonstersToSpawn = 0 then
      Exit;
    repeat
      X := Rnd.Random(Map.SizeX);
      Y := Rnd.Random(Map.SizeY);
    until Sqr(Map.Player.X - X) + Sqr(Map.Player.Y - Y) > Sqr(Map.SizeY / 4);
    if (Map.CanMove(X, Y, MonstersInfo[0].CollisionSize)) then
    begin
      Monster := TMonster.Create;
      repeat
        Monster.Kind := Rnd.Random(MonstersInfo.Count);
      until (CharactersInfo[CurrentCharacter].MonstersSpawnInitialDelay[Monster.Kind] < TotalTime) or (HardcoreModeCache > 1);
      Monster.Inverted := Rnd.RandomBoolean;
      Monster.AnimationTime := 0;
      Monster.IsAlive := true;
      Monster.RandomSize := 0.9 + Rnd.Random * 0.2;
      Monster.X := X + 0.5;
      Monster.Y := Y + 0.5;
      Monster.ActionTimeout := 0;
      for I := 0 to Pred(Map.Monsters.Count) do
        if Sqr(Monster.X - Map.Monsters[I].X) + Sqr(Monster.Y - Map.Monsters[I].Y) < 1 then
        begin
          Monster.Free;
          Exit;
        end;
      Map.Monsters.Add(Monster);
      QueueTutorial(ttMonster, Monster.Kind);
      Dec(MonstersToSpawn);
    end;
  end;

  procedure UpdateMonsters;
  var
    I: Integer;
    DX, DY: Single;
    TargetX, TargetY: Single;
    Speed: Single;
    Mag: Single;

    function MonsterAndBullets: Boolean;
    var
      J: Integer;
      BulletRadius: Single;
    begin
      for J := 0 to Pred(Map.Bullets.Count) do
        if BulletsInfo[Map.Bullets[J].Kind].KillsMonster then
        begin
          if Map.Bullets[J].IsAlive then
            BulletRadius := BulletsInfo[Map.Bullets[J].Kind].InteractionSize
          else
          begin
            BulletRadius := BulletsInfo[Map.Bullets[J].Kind].ExplosionInteractionSize * Map.Bullets[J].AnimationTime / BulletDeathAnimationTime;
            if BulletRadius <= 0 then
              Continue;
          end;
          if Sqr(Map.Bullets[J].X - Map.Monsters[I].X) + Sqr(Map.Bullets[J].Y - Map.Monsters[I].Y) < Sqr((BulletRadius + MonstersInfo[Map.Monsters[I].Kind].InteractionSize) / 2) then
          begin
            if not Map.Bullets[J].IsAlive and (BulletsInfo[Map.Bullets[J].Kind].ExplosionFreeze > 0) then
            begin
              if Map.Monsters[I].Frozen < 0 then
                Map.Monsters[I].Frozen := 0;
              Map.Monsters[I].Frozen += BulletsInfo[Map.Bullets[J].Kind].ExplosionFreeze * SecondsPassed / BulletDeathAnimationTime;
              QueueTutorial(ttMonsterFrozen, -1);
            end else
            begin
              Score += MonstersInfo[Map.Monsters[I].Kind].Reward;
              SoundEngine.Play(SoundEngine.SoundFromName(MonstersInfo[Map.Monsters[I].Kind].DieSound));
              Map.Monsters[I].IsAlive := false;
              Map.Monsters[I].AnimationTime := 0;
              if Map.Bullets[J].IsAlive and not BulletsInfo[Map.Bullets[J].Kind].IsPiercing then
              begin
                Map.Bullets[J].IsAlive := false;
                Map.Bullets[J].AnimationTime := 0;
                SpawnDecal(Map.Bullets[J], BulletsInfo[Map.Bullets[J].Kind].ExplosionDecal);
                if (BulletsInfo[Map.Bullets[J].Kind].Shake > 0) and UserConfig.GetValue('shaker', true) then
                  GameField.Shake(BulletsInfo[Map.Bullets[J].Kind].Shake, 0.3);
                SoundEngine.Play(SoundEngine.SoundFromName(BulletsInfo[Map.Bullets[J].Kind].ImpactSound));
              end;
              Exit(true);
            end;
          end;
        end;
      Result := false;
    end;

    function MonsterCanMove(const AX, AY, MonsterSize: Single): Boolean;
    var
      J: Integer;
    begin
      if Map.Player.Shield > 0 then
        if Sqr(Map.Player.X - AX) + Sqr(Map.Player.Y - AY) < (Map.Player.Shield + 0.5) then
          Exit(false);

      if MonsterSize < 0 then
        Exit(true);

      Result := Map.CanMove(AX, AY, MonsterSize);

      if not Result then
        Exit;
      for J := 0 to Pred(Map.Monsters.Count) do
        if (I <> J) and (MonstersInfo[Map.Monsters[J].Kind].CollisionSize >= 0) and (Sqr(Map.Monsters[J].X - AX) + Sqr(Map.Monsters[J].Y - AY) < Sqr(MonsterSize)) then
          Exit(false);
    end;

    procedure MonsterShootAndDecals;
    var
      Bullet: TBullet;
    begin
      if MonstersInfo[Map.Monsters[I].Kind].ShotKind >= 0 then
      begin
        Map.Monsters[I].ActionTimeout += SecondsPassed;
        if Map.Monsters[I].ActionTimeout > MonstersInfo[Map.Monsters[I].Kind].ActionCooldown then
        begin
          Map.Monsters[I].ActionTimeout -= MonstersInfo[Map.Monsters[I].Kind].ActionCooldown;
          Bullet := TBullet.Create;
          Bullet.Kind := MonstersInfo[Map.Monsters[I].Kind].ShotKind;
          Bullet.AnimationTime := 0;
          Bullet.IsAlive := true;
          Bullet.DX := Map.Player.X - Map.Monsters[I].X;
          Bullet.DY := Map.Player.Y - Map.Monsters[I].Y;
          Mag := Sqrt(Sqr(Bullet.DX) + Sqr(Bullet.DY));
          if Mag = 0 then
          begin
            Bullet.DX := 1;
            Mag := 1;
          end;
          Bullet.DX := Bullet.DX / Mag;
          Bullet.DY := Bullet.DY / Mag;
          Bullet.X := Map.Monsters[I].X + Bullet.DX * MonstersInfo[Map.Monsters[I].Kind].RenderSize / 2;
          Bullet.Y := Map.Monsters[I].Y + Bullet.DY * MonstersInfo[Map.Monsters[I].Kind].RenderSize / 2;
          Map.Bullets.Add(Bullet);
          case BulletsInfo[Bullet.Kind].AffectsPlayer of
            apFreeze: SoundEngine.Play(SoundEngine.SoundFromName('freezeshot'));
            apManaDrain: SoundEngine.Play(SoundEngine.SoundFromName('manalockshot'));
            else
              raise Exception.Create('Unexpected AffectPlayer type');
          end;
        end;
      end
      //NOTE: we can't have both shooting and decals for now, there are few places that would conflict actionTimeout first of all
      else
      if MonstersInfo[Map.Monsters[I].Kind].DecalKind >= 0 then
      begin
        Map.Monsters[I].ActionTimeout += SecondsPassed;
        if Map.Monsters[I].ActionTimeout > MonstersInfo[Map.Monsters[I].Kind].ActionCooldown then
        begin
          Map.Monsters[I].ActionTimeout -= MonstersInfo[Map.Monsters[I].Kind].ActionCooldown;
          SpawnDecal(Map.Monsters[I], MonstersInfo[Map.Monsters[I].Kind].DecalKind);
        end;
      end;
    end;

  begin
    I := 0;
    while I < Map.Monsters.Count do
    begin
      Map.Monsters[I].AnimationTime += SecondsPassed;
      if not Map.Monsters[I].IsAlive then
      begin
        if Map.Monsters[I].AnimationTime > DeathAnimationTime then
          Map.Monsters.Delete(I)
        else
          Inc(I);
        Continue;
      end;
      MonsterShootAndDecals;
      Speed := MonstersInfo[Map.Monsters[I].Kind].Speed * SecondsPassed;
      if Map.Monsters[I].Frozen > 0 then
      begin
        Speed := Speed / 2;
        Map.Monsters[I].Frozen -= SecondsPassed;
      end;
      Map.Monsters[I].IsBuggy -= SecondsPassed;
      if (Map.Monsters[I].IsBuggy < 0) then
      begin
        TargetX := Map.Player.X;
        TargetY := Map.Player.Y;
      end else
      begin
        TargetX := Map.Monsters[I].TargetX;
        TargetY := Map.Monsters[I].TargetY;
      end;
      DX := TargetX - Map.Monsters[I].X;
      DY := TargetY - Map.Monsters[I].Y;
      Mag := Sqrt(Sqr(DX) + Sqr(DY));
      DX += Mag / 2 * Sin(Cos(I + 0.3) * 0.5 * TotalTime);
      DY += Mag / 2 * Cos(Cos(I + 0.3) * 0.5 * TotalTime);
      Mag := Sqrt(Sqr(DX) + Sqr(DY));
      DX := Speed * DX / Mag;
      DY := Speed * DY / Mag;

      if not MonsterCanMove(Map.Monsters[I].X + DX, Map.Monsters[I].Y + DY, MonstersInfo[Map.Monsters[I].Kind].CollisionSize) then
      begin
        if MonsterCanMove(Map.Monsters[I].X, Map.Monsters[I].Y + DY, MonstersInfo[Map.Monsters[I].Kind].CollisionSize) then
          DX := 0
        else
        if MonsterCanMove(Map.Monsters[I].X + DX, Map.Monsters[I].Y, MonstersInfo[Map.Monsters[I].Kind].CollisionSize) then
          DY := 0
        else
        begin
          DX := 0;
          DY := 0;
        end;
      end{ else
        if not MonsterCanMove(Map.Monsters[I].X, Map.Monsters[I].Y + DY, MonstersInfo[Map.Monsters[I].Kind].CollisionSize) and not MonsterCanMove(Map.Monsters[I].X + DX, Map.Monsters[I].Y, MonstersInfo[Map.Monsters[I].Kind].CollisionSize) then //corners
        begin
          DX := 0;
          DY := 0;
        end};
      Mag := Sqrt(Sqr(DX) + Sqr(DY));
      if Mag > Speed / 10 then
      begin
        DX := Speed * DX / Mag;
        DY := Speed * DY / Mag;
        Map.Monsters[I].X += DX;
        Map.Monsters[I].Y += DY;
      end else
      begin
        if Map.Monsters[I].IsBuggy < 0 then // if trying to chase the Player, become buggy
        begin
          Map.Monsters[I].IsBuggy := BuggyTimeout;
          Map.Monsters[I].TargetX := Rnd.Random(Map.SizeX) + 0.5;
          Map.Monsters[I].TargetY := Rnd.Random(Map.SizeY) + 0.5;
        end else
          Map.Monsters[I].IsBuggy := 0; // if already buggy - try to chase player, maybe it's possible now
      end;

      if MonsterAndBullets then
        Continue;

      if Sqr(Map.Player.X - Map.Monsters[I].X) + Sqr(Map.Player.Y - Map.Monsters[I].Y) < Sqr((CharactersInfo[CurrentCharacter].InteractionSize + MonstersInfo[Map.Monsters[I].Kind].InteractionSize) / 2) then
      begin
        HitPlayer;
        Map.Monsters[I].IsAlive := false;
        Map.Monsters[I].AnimationTime := 0;
        Continue;
      end;
      Inc(I);
    end;
  end;

  procedure UpdateTutorial;

    procedure ShowBottomTutorial(const AX, AY: Integer; const AHeader, AText: String);
    begin
      TutorialImage.SetImageRect(AX, AY);
      TutorialBottomHeaderLabel.Caption := AHeader;
      TutorialBottomTextLabel.Caption := AText;
      IsTutorialBottom := true;
    end;

    procedure ShowTopTutorial(const AHeader, AText: String);
    begin
      TutorialTopHeaderLabel.Caption := AHeader;
      TutorialTopTextLabel.Caption := AText;
      IsTutorialBottom := false;
    end;

  begin
    TutorialTimer -= SecondsPassed;
    if (TutorialTimer < 0) and (TutorialQueue.Count > 0) then
    begin
      case TutorialQueue[0].TutorialType of
        ttPowerup: ShowBottomTutorial(1, PowerupsInfo[TutorialQueue[0].Entry].RenderTile, PowerupsInfo[TutorialQueue[0].Entry].Name, PowerupsInfo[TutorialQueue[0].Entry].Description);
        ttMonster: ShowBottomTutorial(2, MonstersInfo[TutorialQueue[0].Entry].RenderTile, MonstersInfo[TutorialQueue[0].Entry].Name, MonstersInfo[TutorialQueue[0].Entry].Description);
        ttDecal: ShowBottomTutorial(1, DecalsInfo[TutorialQueue[0].Entry].RenderTile, DecalsInfo[TutorialQueue[0].Entry].Name, DecalsInfo[TutorialQueue[0].Entry].Description);
        ttMonsterFrozen: ShowBottomTutorial(1, 22, 'Monster is frozen!', 'Frozen monsters move twice slower.');
        ttPlayerFrozen: ShowBottomTutorial(1, 22, 'Heroine is frozen!', 'She moves and recharges spell twice slower.');
        ttPlayerShield: ShowBottomTutorial(1, 21, 'Shield', 'Keeps monsters away. Careful, the character can still step on them and get hurt.');
        ttNoMana: ShowTopTutorial('Out of mana!', 'Wait until it regenerates or pick up a powerup.');
        ttPlayerDash: ShowTopTutorial('Dash ability', 'The character moves at dash speed until dash charge (the circle near the mana bar) is depleted. Stand still to refill.');
        ttPlayerDashThroughWalls: ShowTopTutorial('Ability to pass through walls', 'Move into the wall, if dash charge is enough the character will teleport to the other side.');
        ttPlayerManaLock: ShowTopTutorial('Mana lock!', 'Character''s mana regeneration is blocked.');
        ttPlayerSlowRecharge: ShowTopTutorial('Slow spell recharge', 'Spell recharges longer than half a second, pay attention to bright circular meter.');
        else
          raise Exception.Create('Unexpected TutorialType');
      end;
      TutorialQueue.Delete(0);
      TutorialTimer := TutorialTimeout;
      SoundEngine.Play(SoundEngine.SoundFromName('tutorial'));
    end;
    TutorialBottom.Exists := IsTutorialBottom and (TutorialTimer >= 0);
    TutorialTop.Exists := not IsTutorialBottom and (TutorialTimer >= 0);
  end;

begin
  inherited;
  if GameOn then
  begin
    TotalTime += SecondsPassed;

    PlayerManaRegeneration;
    PlayerRecharge;
    MovePlayer;
    PlayerAndBullets;
    FireBullets;
    UpdateBullets;
    UpdateDecals;
    SpawnPowerups;
    UpdatePowerups;
    SpawnMonsters;
    UpdateMonsters;

    ScoreLabel.Caption := Score.ToString;
    HighscoreCurrentImage.Exists := GameField.Exists and (Score > HighScore);
    HighscoreBeforeImage.Exists := GameField.Exists and (Highscore >= Score) and (Highscore > 0);
  end;

  UpdateTutorial;
end;

end.
