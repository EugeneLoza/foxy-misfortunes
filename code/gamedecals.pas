{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameDecals;

{$mode ObjFPC}{$H+}

interface

uses
  Generics.Collections, DOM;

type
  TDecalEffect = (deUndefined, deNone, deFreeze, deManaLock);

type
  TDecalInfo = class(TObject)
  public
    Id: Integer;
    Name: String;
    Description: String;
    RenderTile: Integer;
    RenderSize: Single;
    InteractionSize: Single;
    Sound: String;
    Effect: TDecalEffect;
    EffectStrength: Single;
    CanBeCleared: Boolean;
    constructor Create(const Element: TDOMElement);
  end;
  TDecalsList = specialize TObjectList<TDecalInfo>;

var
  DecalsInfo: TDecalsList;

procedure InitDecals;

implementation
uses
  SysUtils, CastleXmlUtils;

procedure InitDecals;
var
  NewDecal: TDecalInfo;
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
begin
  DecalsInfo := TDecalsList.Create(true);

  Doc := URLReadXML('castle-data:/map/decals.xml');
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('decal');
    try
      while Iterator.GetNext do
      begin
        NewDecal := TDecalInfo.Create(Iterator.Current);
        if NewDecal.Id <> DecalsInfo.Count then
          raise Exception.CreateFmt('Unexpected decal Id: %d (expected: %d) - All items must have unique IDs identical to their order', [NewDecal.Id, DecalsInfo.Count]);
        DecalsInfo.Add(NewDecal);
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;
end;

constructor TDecalInfo.Create(const Element: TDOMElement);

  function StrToDecalEffect(const AString: String): TDecalEffect;
  begin
    case AString of
      'deNone': Result := deNone;
      'deFreeze': Result := deFreeze;
      'deManaLock': Result := deManaLock;
      else
        raise Exception.Create('Unexpected DecalEffect: ' + AString);
    end;
  end;

begin
  //inherited - empty
  Id := Element.AttributeInteger('Id');
  Name := Element.AttributeString('Name');
  Description := Element.AttributeString('Description');
  RenderTile := Element.AttributeInteger('RenderTile');
  RenderSize := Element.AttributeFloat('RenderSize');
  InteractionSize := Element.AttributeFloat('InteractionSize');
  Sound := Element.AttributeString('Sound');
  Effect := StrToDecalEffect(Element.AttributeString('Effect'));
  EffectStrength := Element.AttributeFloat('EffectStrength');
  CanBeCleared := Element.AttributeBoolean('CanBeCleared');
end;

finalization
  DecalsInfo.Free;
end.

