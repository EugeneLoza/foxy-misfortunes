{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameCharacters;

{$mode ObjFPC}{$H+}

interface

uses
  Generics.Collections, DOM;

const
  MaxCharHp = 6;
  SkinUnlockScore = 1000;

const
  SkincasterManaBonus = 12;
  SkincasterRechargeBonus = 0.2;
  SkincasterSpeedBonus = 0.2;
  SkincasterBulletCostBonus = 0.4;
  SkincasterManaLockResistanceBonus = 0.25;

type
  TCharacterSkin = class(TObject)
    Name: String;
    BaseImg: String;
    CensoredImg: String;
    HpImg: array[0..MaxCharHp] of String;
    OverlayImg: String;
    RenderTileX: Integer;
    RenderTileY: Integer;
    BulletKind: Integer;
    constructor Create(const Element: TDOMElement);
  end;
  TSkinsList = specialize TObjectList<TCharacterSkin>;

  TCharacterInfo = class(TObject)
  public
    Id: Integer;
    IsLava: Boolean;
    IsSkincaster: Boolean;
    Name: String;
    Story: String;
    Skins: TSkinsList;
    StyleDescription: String;
    SpellDescription: String;
    SpecialDescription: String;
    MaxMana: Single;
    Speed: Single;
    Dash: Single;
    DashSpeed: Single;
    DashRecharge: Single;
    DashThroughWalls: Boolean;
    // in seconds of mana regeneration
    BulletCost: Single;
    // delay between shots, in seconds
    RechargeSpeed: Single;
    // Add shield when shooting
    ShieldOnShot: Single;
    // Add shield when shooting
    SpeedOnShot: Single;
    MaxMonsterSpawnDelay: Single;
    MinMonsterSpawnDelay: Single;
    MenuHp: Integer;
    MaxHp: Integer;
    RenderSize: Single;
    InteractionSize: Single;
    CollisionSize: Single;
    PowerupsSpawnDelay: array of Single;
    PowerupsSpawnInitialDelay: array of Single;
    PowerupsSpawnInitialQuantity: array of Integer;
    MonstersSpawnInitialDelay: array of Single;
    ScoreToUnlockNextCharacter: Integer;
    StripExplosion: Integer;
    StripBulletKind: Integer;
    StripExplosionRadius: Single;
    StripFreeze: Single;
    StripReward: Integer;
    StripRecharge: Single;
    StripShield: Single;
    StripSpeedBoost: Single;
    StripMana: Single;
    FreezeResistance: Single;
    ManaLockResistance: Single;
    function EffectiveManaRegen: Single;
    function ScoreToUnlockNextSkin: Integer;
    function CharacterSummary: String;
    constructor Create(const Element: TDOMElement);
    destructor Destroy; override;
  end;
  TCharactersList = specialize TObjectList<TCharacterInfo>;

var
  CharactersInfo: TCharactersList;
  CurrentCharacter: Integer;
  SelectedCharacter: Integer;
  CurrentSkin: Integer;
  SelectedSkin: Integer;

procedure InitCharacters;

implementation
uses
  SysUtils,
  CastleUtils, CastleConfig, CastleXmlUtils,
  GamePowerups, GameMonsters;

function RoundToString(const AValue: Single): String;
begin
  if Round(AValue) = Round(AValue * 10) / 10.0 then
    Result := Round(AValue).ToString + '.0'
  else
    Result := FloatToStr(Round(AValue * 10) / 10);
end;

function TCharacterInfo.EffectiveManaRegen: Single;
var
  I: Integer;
begin
  Result := 1;
  for I := 0 to Pred(PowerupsInfo.Count) do
    if PowerupsInfo[I].Effect = peMana then
      Result += PowerupsInfo[I].EffectStrength / PowerupsSpawnDelay[I];
end;

function TCharacterInfo.ScoreToUnlockNextSkin: Integer;
var
  NextSkinScoreBonus: Integer;
begin
  NextSkinScoreBonus := SkinUnlockScore * (UserConfig.GetValue('max_unlocked_skin_' + CurrentCharacter.ToString, 0) + 1);
  {$ifdef Android}
  NextSkinScoreBonus := NextSkinScoreBonus div 2;
  {$endif}
  Result := ScoreToUnlockNextCharacter + NextSkinScoreBonus;
end;

function TCharacterInfo.CharacterSummary: String;
var
  IsLastCharacter: Boolean;
  CanUnlockNext: Boolean;
  CanUnlockSkin: Boolean;
begin
  Result :=
    'Name: ' + Name + NL +
    'Style: ' + StyleDescription + NL +
    'Hit Points: ' + MaxHp.ToString + ' pcs' + NL +
    'Spell: ' + SpellDescription + NL;
  if not IsSkincaster then
    Result +=
      'Spell recharge: ' + RoundToString(RechargeSpeed) + ' secs' + NL +
      'Spell cost: ' + RoundToString(BulletCost) + ' secs per shot' + NL +
      'Mana pool: ' + Round(MaxMana).ToString + ' secs (' + RoundToString(MaxMana / BulletCost) + ' shots)' + NL +
      'Movement Speed: ' + RoundToString(Speed) + NL
  else
    Result +=
      'Spell recharge: ' + RoundToString(RechargeSpeed) + ' - ' + RoundToString(RechargeSpeed - SkincasterRechargeBonus * (MaxHp-1)) + ' secs' + NL +
      'Spell cost: ' + RoundToString(BulletCost) + ' - ' + RoundToString(BulletCost - SkincasterBulletCostBonus * (MaxHp-1)) + ' secs per shot' + NL +
      'Mana pool: ' + Round(MaxMana).ToString + ' secs (' + RoundToString(MaxMana / BulletCost) + ' shots)' + NL +
      '                   - ' + Round(MaxMana + SkincasterManaBonus * (MaxHp-1)).ToString + ' secs (' + RoundToString((MaxMana + SkincasterManaBonus * (MaxHp-1)) / (BulletCost - SkincasterBulletCostBonus * (MaxHp-1))) + ' shots)' + NL +
      'Movement Speed: ' + RoundToString(Speed) + ' - ' + RoundToString(Speed + SkincasterSpeedBonus * (MaxHp-1)) + NL;

  if Dash > 0 then
    Result +=
      'Dash: ' + RoundToString(Dash) + ' secs' + NL +
      'Dash speed: ' + RoundToString(DashSpeed) + NL +
      'Dash recharge: ' + RoundToString(Dash / DashRecharge) + ' secs' + NL;

  if DashThroughWalls then
    Result +=
     'This character can dash through walls!' + NL;

  if Dash > 0 then
    Result +=
      NL +
      'Using Dash: The character moves at dash speed until the dash meter (circle near the mana bar) is depleted. Stand still to refill.' +
      NL + NL;

  Result += 'Special: ' + SpecialDescription;

  IsLastCharacter := Id = CharactersInfo.Count - 1;
  CanUnlockNext := Id = UserConfig.GetValue('max_unlocked_character', 0);
  CanUnlockSkin := UserConfig.GetValue('max_unlocked_skin_' + Id.ToString, 0) < Skins.Count - 1;

  if not CanUnlockNext or IsLastCharacter then
  if CanUnlockSkin then
  begin
    Result +=
      NL + NL +
      'Earn ' + ScoreToUnlockNextSkin.ToString + ' points to unlock a new skin for ' + Name + '!';
  end else
    Result +=
      NL + NL +
      'You''ve unlocked everything this character has to offer in this version of the game. Congratulations!' + NL +
      'You can still try to beat your own high score.';

  if IsLastCharacter then
  begin
    if UserConfig.GetValue('game_won', false) then
      Result +=
        NL + NL +
        'This is the last character in this game!' + NL +
        'Thank you for playing!' + NL
    else
      Result +=
        NL + NL +
        'This is the last character in this game!' + NL +
        'Goal: Earn ' + ScoreToUnlockNextCharacter.ToString + ' points as ' + Name + ' to win the game!';
  end else
  if CanUnlockNext then
    Result +=
      NL + NL +
      'Goal: Earn ' + ScoreToUnlockNextCharacter.ToString + ' points as ' + Name + ' to unlock the next character!';
end;

constructor TCharacterSkin.Create(const Element: TDOMElement);
var
  I: Integer;
begin
  //inherited - empty
  Name := Element.AttributeString('Name');
  BaseImg := Element.AttributeString('BaseImg');
  CensoredImg := Element.AttributeString('CensoredImg');
  for I := 0 to MaxCharHp do
    HpImg[I] := Element.AttributeStringDef('HpImg_' + I.ToString, '');
  OverlayImg := Element.AttributeString('OverlayImg');
  RenderTileX := Element.AttributeInteger('RenderTileX');
  RenderTileY := Element.AttributeInteger('RenderTileY');
  BulletKind := Element.AttributeInteger('BulletKind');
end;

constructor TCharacterInfo.Create(const Element: TDOMElement);
var
  NewSkin: TCharacterSkin;
  Iterator: TXMLElementIterator;
  I: Integer;
begin
  //inherited - empty
  Id := Element.AttributeInteger('Id');
  Name := Element.AttributeString('Name');
  IsLava := Element.AttributeBoolean('IsLava');
  IsSkincaster := Element.AttributeBoolean('IsSkincaster');
  Story := '      ' + Element.Child('story').TextContent;
  StyleDescription := Element.AttributeString('StyleDescription');
  SpellDescription := Element.AttributeString('SpellDescription');
  SpecialDescription := Element.AttributeString('SpecialDescription');
  MaxMana := Element.AttributeFloat('MaxMana');
  Speed := Element.AttributeFloat('Speed');
  Dash := Element.AttributeFloat('Dash');
  DashSpeed := Element.AttributeFloat('DashSpeed');
  DashRecharge := Element.AttributeFloat('DashRecharge');
  DashThroughWalls := Element.AttributeBoolean('DashThroughWalls');
  BulletCost := Element.AttributeFloat('BulletCost');
  RechargeSpeed := Element.AttributeFloat('RechargeSpeed');
  ShieldOnShot := Element.AttributeFloat('ShieldOnShot');
  SpeedOnShot := Element.AttributeFloat('SpeedOnShot');
  MaxMonsterSpawnDelay := Element.AttributeFloat('MaxMonsterSpawnDelay');
  MenuHp := Element.AttributeInteger('MenuHp');
  MaxHp := Element.AttributeInteger('MaxHp');
  RenderSize := Element.AttributeFloat('RenderSize');
  InteractionSize := Element.AttributeFloat('InteractionSize');
  CollisionSize := Element.AttributeFloat('CollisionSize');
  ScoreToUnlockNextCharacter := Element.AttributeInteger('ScoreToUnlockNextCharacter');
  {$ifdef Android}ScoreToUnlockNextCharacter := ScoreToUnlockNextCharacter div 2;{$endif}
  StripExplosion := Element.AttributeInteger('StripExplosion');
  StripBulletKind := Element.AttributeInteger('StripBulletKind');
  StripExplosionRadius := Element.AttributeFloat('StripExplosionRadius');
  StripFreeze := Element.AttributeFloat('StripFreeze');
  StripReward := Element.AttributeInteger('StripReward');
  StripRecharge := Element.AttributeFloat('StripRecharge');
  StripShield := Element.AttributeFloat('StripShield');
  StripSpeedBoost := Element.AttributeFloat('StripSpeedBoost');
  StripMana := Element.AttributeFloat('StripMana');
  FreezeResistance := Element.AttributeFloat('FreezeResistance');
  ManaLockResistance := Element.AttributeFloat('ManaLockResistance');

  SetLength(PowerupsSpawnDelay, PowerupsInfo.Count);
  SetLength(PowerupsSpawnInitialDelay, PowerupsInfo.Count);
  SetLength(PowerupsSpawnInitialQuantity, PowerupsInfo.Count);
  for I := 0 to Pred(PowerupsInfo.Count) do
  begin
    PowerupsSpawnDelay[I] := Element.AttributeFloat('PowerupsSpawnDelay_' + I.ToString);
    PowerupsSpawnInitialDelay[I] := Element.AttributeFloat('PowerupsSpawnInitialDelay_' + I.ToString);
    PowerupsSpawnInitialQuantity[I] := Element.AttributeInteger('PowerupsSpawnInitialQuantity_' + I.ToString);
  end;
  SetLength(MonstersSpawnInitialDelay, MonstersInfo.Count);
  for I := 0 to Pred(MonstersInfo.Count) do
    MonstersSpawnInitialDelay[I] := Element.AttributeFloat('MonstersSpawnInitialDelay_' + I.ToString);

  MinMonsterSpawnDelay := BulletCost / EffectiveManaRegen * 0.85;

  Skins := TSkinsList.Create(true);
  Iterator := Element.ChildrenIterator('skin');
  try
    while Iterator.GetNext do
    begin
      NewSkin := TCharacterSkin.Create(Iterator.Current);
      Skins.Add(NewSkin);
    end;
  finally FreeAndNil(Iterator) end;
end;

destructor TCharacterInfo.Destroy;
begin
  Skins.Free;
  inherited Destroy;
end;

procedure InitCharacters;
var
  NewCharacter: TCharacterInfo;
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
begin
  CharactersInfo := TCharactersList.Create(true);
  Doc := URLReadXML('castle-data:/characters/characters.xml');
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('character');
    try
      while Iterator.GetNext do
      begin
        NewCharacter := TCharacterInfo.Create(Iterator.Current);
        if NewCharacter.Id <> CharactersInfo.Count then
          raise Exception.CreateFmt('Unexpected character Id: %d (expected: %d) - All items must have unique IDs identical to their order', [NewCharacter.Id, CharactersInfo.Count]);
        CharactersInfo.Add(NewCharacter);
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;
end;

finalization
  CharactersInfo.Free;
end.

