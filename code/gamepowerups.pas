{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GamePowerups;

{$mode ObjFPC}{$H+}

interface

uses
  Generics.Collections, DOM;

type
  TPowerupEffect = (peUndefined, peNone, peMana, peShield, peSpeed);

type
  TPowerupInfo = class(TObject)
  public
    Id: Integer;
    Name: String;
    Description: String;
    RenderTile: Integer;
    RenderSize: Single;
    InteractionSize: Single;
    Reward: Integer;
    Sound: String;
    Effect: TPowerupEffect;
    EffectStrength: Single;
    constructor Create(const Element: TDOMElement);
  end;
  TPowerupsList = specialize TObjectList<TPowerupInfo>;

var
  PowerupsInfo: TPowerupsList;

procedure InitPowerups;

implementation
uses
  SysUtils, CastleXmlUtils;

procedure InitPowerups;
var
  NewPowerup: TPowerupInfo;
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
begin
  PowerupsInfo := TPowerupsList.Create(true);
  Doc := URLReadXML('castle-data:/map/powerups.xml');
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('powerup');
    try
      while Iterator.GetNext do
      begin
        NewPowerup := TPowerupInfo.Create(Iterator.Current);
        if NewPowerup.Id <> PowerupsInfo.Count then
          raise Exception.CreateFmt('Unexpected powerup Id: %d (expected: %d) - All items must have unique IDs identical to their order', [NewPowerup.Id, PowerupsInfo.Count]);
        PowerupsInfo.Add(NewPowerup);
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;
end;


constructor TPowerupInfo.Create(const Element: TDOMElement);

  function StrToPowerupEffect(const AString: String): TPowerupEffect;
  begin
    case AString of
      'peNone': Result := peNone;
      'peMana': Result := peMana;
      'peShield': Result := peShield;
      'peSpeed': Result := peSpeed;
      else
        raise Exception.Create('Unexpected PowerupEffect: ' + AString);
    end;
  end;

begin
  //inherited - empty
  Id := Element.AttributeInteger('Id');
  Name := Element.AttributeString('Name');
  Description := Element.AttributeString('Description');
  RenderTile := Element.AttributeInteger('RenderTile');
  RenderSize := Element.AttributeFloat('RenderSize');
  InteractionSize := Element.AttributeFloat('InteractionSize');
  Reward := Element.AttributeInteger('Reward');
  Sound := Element.AttributeString('Sound');
  Effect := StrToPowerupEffect(Element.AttributeString('Effect'));
  EffectStrength := Element.AttributeFloat('EffectStrength');
end;

finalization
  PowerupsInfo.Free;
end.

