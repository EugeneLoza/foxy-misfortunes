{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameTutorial;

{$mode ObjFPC}{$H+}

interface

uses
  Generics.Collections;

const
  TutorialTimeout = 10;

type
  TTutorialType = (ttUndefined,
    ttPowerup, ttMonster, ttDecal,
    ttMonsterFrozen, ttPlayerFrozen, ttPlayerShield,
    ttPlayerDash, ttPlayerDashThroughWalls,
    ttPlayerSlowRecharge, ttPlayerManaLock, ttNoMana);

type
  TTutorialRecord = record
    TutorialType: TTutorialType;
    Entry: Integer;
  end;
  TTutorialQueue = specialize TList<TTutorialRecord>;

var
  TutorialQueue: TTutorialQueue;

function TutorialTypeToString(ATutorialType: TTutorialType): String;
implementation
uses
  SysUtils, typinfo;

function TutorialTypeToString(ATutorialType: TTutorialType): String;
begin
  Result := GetEnumName(TypeInfo(TTutorialType), Ord(ATutorialType));
end;

initialization
  TutorialQueue := TTutorialQueue.Create;
finalization
  TutorialQueue.Free;
end.

