{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameTileImage;

interface

uses
  Classes,
  CastleUiControls, CastleControls, CastleGlImages, CastleRectangles;

const
  MaxSizeX = 30;
  MaxSizeY = 23;

type
  TTileImage = class(TCastleUserInterface)
  public
    Texture: TDrawableImage;
    ImageRect: TFloatRectangle;
    procedure SetImageRect(const AX, AY: Integer);
    procedure Render; override;
  end;

implementation

procedure TTileImage.SetImageRect(const AX, AY: Integer);
begin
  ImageRect := FloatRectangle(AX * 64 + 1, AY * 64 + 1, 64 - 2, 64 - 2);
end;

procedure TTileImage.Render;
begin
  inherited; // parent is empty
  Texture.Draw(RenderRect, ImageRect);
end;

end.

