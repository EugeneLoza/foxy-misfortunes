unit CastleUiShaker;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  CastleUiControls, CastleVectors, CastleClassUtils,
  GameRandom;

type
  TShakeCurve = (
    { Shaking lasts longer }
    scLinear,
    { Shaking fades out quicker }
    scSquare);
  TShakeType = (
    { Just apply jitter to children of this UI. Very fast, but not very nice looking. }
    stRandom,
    { Smooth randomness a bit. Still quick enough but better than stRandom. }
    stRandomSmooth,
    { Slower but much smoother than random variants. }
    stNoise,
    { Slowest of the shaking methods, but most smooth. Maybe a bit too smooth. }
    stNoiseSmooth);

type
  { Note: it's a good idea to give an option for the User to turn screen shaking off }
  TCastleUiShaker = class(TCastleUserInterface)
  strict private const
    DefaultShakeCurve = scSquare;
    DefaultShakeType = stRandomSmooth;
    DefaultNoisiness = Single(10);
  strict private
    FShakeCurve: TShakeCurve;
    FShakeType: TShakeType;
    FNoisiness: Single;
    ShakeAmplitude: Single;
    IsShaking: Boolean;
    ShakeTime: Single;
    ShakeTimeout: Single;
    SeedX, SeedY: Cardinal;
    AnchorDeltaStore: TVector2;
    function GetAmplitudeFade: Single;
  public
    { Call this to shake children of this UI
      Amplitude: How severe the shaking is.
      Duration: How long shaking happens. See also ShakeCurve.
      WARNING: Do not modify this component's anchors when the screen is shaking,
      the new values will be discarded. }
    procedure Shake(const AAmplitude: Single; const ADuration: Single);
    procedure StopShake;
    function PropertySections(const PropertyName: String): TPropertySections; override;
    procedure Update(const SecondsPassed: Single;
      var HandleInput: Boolean); override;
    constructor Create(AOwner: TComponent); override;
  published
    { How quickly shaking fades - linearnly (scLinear) or by parabola (scSquare). }
    property ShakeCurve: TShakeCurve read FShakeCurve write FShakeCurve default DefaultShakeCurve;
    { Shaking algorithm. }
    property ShakeType: TShakeType read FShakeType write FShakeType default DefaultShakeType;
    { How noisy is shaking.
      Used only for stNoise and stNoiseSmooth }
    property Noisiness: Single read FNoisiness write FNoisiness default DefaultNoisiness;
  end;

implementation
uses
  SysUtils, Math,
  CastleLog, CastleInternalNoise,
  CastleComponentSerialize;

constructor TCastleUiShaker.Create(AOwner: TComponent);
begin
  inherited;
  FShakeCurve := DefaultShakeCurve;
  FShakeType := DefaultShakeType;
  FNoisiness := DefaultNoisiness;
end;

function TCastleUiShaker.GetAmplitudeFade: Single;
begin
  case FShakeCurve of
    scLinear: Result := ShakeTimeout / ShakeTime;
    scSquare: Result := Sqr(ShakeTimeout / ShakeTime);
  end;
end;

procedure TCastleUiShaker.Update(const SecondsPassed: Single;
      var HandleInput: Boolean);

  function Noise: TVector2;
  var
    T, S, C: Single;
  begin
    T := 2 * Pi * ShakeTimeout / ShakeTime;
    S := FNoisiness * Sin(T);
    C := FNoisiness * Cos(T);
    Result := Vector2(InterpolatedNoise2D_Spline(S, C, SeedX) - 0.5, InterpolatedNoise2D_Spline(S, C, SeedY) - 0.5)
  end;

  function SmoothNoise: TVector2;
  var
    T, S, C: Single;
  begin
    T := 2 * Pi * ShakeTimeout / ShakeTime;
    S := FNoisiness * Sin(T);
    C := FNoisiness * Cos(T);
    Result := Vector2(BlurredInterpolatedNoise2D_Spline(S, C, SeedX) - 0.5, BlurredInterpolatedNoise2D_Spline(S, C, SeedY) - 0.5)
  end;

begin
  inherited;
  if IsShaking then
  begin
    if FullSize then
    begin
      WriteLnWarning('Unable to shake screen if the UiShaker size is FullSize');
      IsShaking := false;
      Exit;
    end;
    ShakeTimeout -= SecondsPassed;
    if ShakeTimeout > 0 then
    begin
      case FShakeType of
        stRandom: AnchorDelta := AnchorDeltaStore + ShakeAmplitude * GetAmplitudeFade * 2 * Vector2((Rnd.Random - 0.5), (Rnd.Random - 0.5));
        stRandomSmooth: AnchorDelta := (AnchorDelta + AnchorDeltaStore + ShakeAmplitude * GetAmplitudeFade * 2 * Vector2((Rnd.Random - 0.5), (Rnd.Random - 0.5))) / 2;
        stNoise: AnchorDelta := AnchorDeltaStore + ShakeAmplitude * GetAmplitudeFade * 2 * Noise;
        stNoiseSmooth: AnchorDelta := AnchorDeltaStore + ShakeAmplitude * GetAmplitudeFade * 2 * SmoothNoise;
      end;
    end else
      StopShake;
  end;
end;

function TCastleUiShaker.PropertySections(
  const PropertyName: String): TPropertySections;
begin
  case PropertyName of
    'ShakeCurve', 'ShakeType', 'Noisiness':
      Result := [psBasic]
    else
      Result := inherited PropertySections(PropertyName);
  end;
end;

procedure TCastleUiShaker.Shake(const AAmplitude: Single; const ADuration: Single);
begin
  if not IsShaking then
  begin
    AnchorDeltaStore := AnchorDelta;
    ShakeTime := ADuration;
    ShakeAmplitude := AAmplitude;
    IsShaking := true;
  end else
  begin
    // Blending multiple shakes
    ShakeTime := Max(ADuration, ShakeTimeout) + Min(ADuration, ShakeTimeout) / 3;
    ShakeAmplitude := Max(AAmplitude, ShakeAmplitude * GetAmplitudeFade) + Min(AAmplitude, ShakeAmplitude * GetAmplitudeFade) / 2;
  end;
  if ShakeType = stNoise then
  begin
    SeedX := Rnd.Random(100); // in IntegerNoiseCore only 100 prime numbers
    SeedY := Rnd.Random(100);
  end;
  ShakeTimeout := ShakeTime;
end;

procedure TCastleUiShaker.StopShake;
begin
  AnchorDelta := AnchorDeltaStore;
  IsShaking := false;
end;

initialization
  RegisterSerializableComponent(TCastleUiShaker, 'UI Shaker');
end.

