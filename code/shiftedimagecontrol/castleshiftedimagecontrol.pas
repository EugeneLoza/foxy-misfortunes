unit CastleShiftedImageControl;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  CastleControls, CastleUiControls, CastleColors, CastleClassUtils,
  CastleShiftedImage;

type
  { Mostly a copy from TCastleImageControl }
  TCastleShiftedImageControl = class(TCastleUserInterface)
  strict private
    FStretch: Boolean;
    FProportionalScaling: TProportionalScaling;
    FContent: TCastleShiftedImage{Persistent};

    function GetUrl: String;
    function GetColor: TCastleColor;
    procedure SetURL(const Value: String);
    procedure SetColor(const Value: TCastleColor);

    procedure SetStretch(const Value: boolean);
    procedure SetProportionalScaling(const Value: TProportionalScaling);

    procedure ContentChanged(Sender: TObject);
  protected
    procedure PreferredSize(var PreferredWidth, PreferredHeight: Single); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Render; override;
    procedure EditorAllowResize(
      out ResizeWidth, ResizeHeight: Boolean; out Reason: String); override;
    function PropertySections(const PropertyName: String): TPropertySections; override;

    { Color tint of the image. This simply multiplies the image RGBA components,
      just like @link(TDrawableImage.Color). By default this is opaque white,
      which means that image colors are unchanged.

      @seealso TCastleImagePersistent.Color }
    property Color: TCastleColor read GetColor write SetColor;
  published
    { Image contents to display. }
    property Content: TCastleShiftedImage read FContent;

    { URL of the image.
      Set this to load a new image, you can set to '' to clear the image.

      @seealso TCastleImagePersistent.URL }
    property URL: string read GetURL write SetURL;

    { How does the loaded image size affect the size of the image control.

      @unorderedList(
        @item(
          When @link(FullSize) or @link(AutoSizeToChildren) is @true,
          then the value of @link(Stretch), and loaded image size, are ignored.
          When @link(AutoSizeToChildren),
          the image is always stretched to surround the children.
          When @link(FullSize),
          the image is always stretched to fill the parent.

          Otherwise:
        )

        @item(If Stretch = @false (default), then
          the displayed size corresponds to the underlying image size.)

        @item(If Stretch = @true, the image will be stretched to fill
          the requested area. The property
          @link(ProportionalScaling) determines how the image will
          be adjusted to fit the requested size
          (by @link(Width), @link(WidthFraction), @link(Height), @link(HeightFraction)).

          @definitionList(
            @itemLabel psNone
            @item(The image will be scaled to exactly fill
              the requested Width and Height
              (without paying attention to the aspect ratio of the image).

              This is the case when you fully force the displayed size
              and position, regardless of image size. Displayed image will
              always exactly fill the requested area.)

            @itemLabel psFit
            @item(The image will be proportionally scaled to fit within
              the requested Width and Height.
              If the aspect ratio of image
              will be different than aspect ratio of Width/Height, the scaled image
              will be centered inside the Width/Height.)

            @itemLabel psEnclose
            @item(The image will be proportionally scaled to enclode
              the requested Width and Height.
              If the aspect ratio of image
              will be different than aspect ratio of Width/Height, the scaled image
              will be larger then the requested area.)
          )
        )
      )

      @groupBegin }
    property Stretch: boolean read FStretch write SetStretch default false;
    property ProportionalScaling: TProportionalScaling
      read FProportionalScaling write SetProportionalScaling default psNone;
    { @groupEnd }
  end;

implementation
uses
  SysUtils,
  CastleUtils, CastleStringUtils, CastleComponentSerialize
  {$ifdef CASTLE_DESIGN_MODE} , PropEdits, CastlePropEdits {$endif};

constructor TCastleShiftedImageControl.Create(AOwner: TComponent);
begin
  inherited;
  FContent := TCastleShiftedImage.Create;
  FContent.OnChange := @ContentChanged;
end;

destructor TCastleShiftedImageControl.Destroy;
begin
  FreeAndNil(FContent);
  inherited;
end;

procedure TCastleShiftedImageControl.Render;
begin
  inherited; // empty
  FContent.Draw(RenderRect);
end;

procedure TCastleShiftedImageControl.PreferredSize(var PreferredWidth, PreferredHeight: Single);
var
  ImageWidth, ImageHeight: Single;
begin
  inherited;

  ImageWidth  := FContent.Width;
  ImageHeight := FContent.Height;

  if not Stretch then
  begin
    PreferredWidth  := ImageWidth  * UIScale;
    PreferredHeight := ImageHeight * UIScale;
  end else
  if (ProportionalScaling in [psFit, psEnclose]) and
     (ImageWidth <> 0) and
     (ImageHeight <> 0) then
  begin
    if (ProportionalScaling = psFit) =
       (PreferredWidth / PreferredHeight >
        ImageWidth / ImageHeight) then
    begin
      PreferredWidth := ImageWidth * PreferredHeight / ImageHeight;
    end else
    begin
      PreferredHeight := ImageHeight * PreferredWidth / ImageWidth;
    end;
  end;
end;

procedure TCastleShiftedImageControl.EditorAllowResize(
  out ResizeWidth, ResizeHeight: Boolean; out Reason: String);
begin
  inherited;
  if not Stretch then
  begin
    ResizeWidth := false;
    ResizeHeight := false;
    Reason := SAppendPart(Reason, NL, 'Turn on "TCastleShiftedImageControl.Stretch" to change size.');
  end;
end;

function TCastleShiftedImageControl.PropertySections(
  const PropertyName: String): TPropertySections;
begin
  case PropertyName of
    'URL', 'Stretch', 'ProportionalScaling'{, 'ColorPersistent', 'SmoothScaling', 'AlphaChannel',
    'FlipHorizontal', 'FlipVertical', 'ProtectedSides', 'Rotation'}:
      Result := [psBasic]
    else
      Result := inherited PropertySections(PropertyName);
  end;
end;

procedure TCastleShiftedImageControl.ContentChanged(Sender: TObject);
begin
  VisibleChange([chRectangle]);
end;

procedure TCastleShiftedImageControl.SetStretch(const Value: boolean);
begin
  if FStretch <> Value then
  begin
    FStretch := Value;
    VisibleChange([chRectangle]);
  end;
end;

procedure TCastleShiftedImageControl.SetProportionalScaling(const Value: TProportionalScaling);
begin
  if FProportionalScaling <> Value then
  begin
    FProportionalScaling := Value;
    VisibleChange([chRectangle]);
  end;
end;

function TCastleShiftedImageControl.GetURL: String;
begin
  Result := FContent.URL;
end;

procedure TCastleShiftedImageControl.SetURL(const Value: String);
begin
  FContent.URL := Value;
end;

function TCastleShiftedImageControl.GetColor: TCastleColor;
begin
  Result := FContent.Color;
end;

procedure TCastleShiftedImageControl.SetColor(const Value: TCastleColor);
begin
  FContent.Color := Value;
end;

initialization
  RegisterSerializableComponent(TCastleShiftedImageControl, 'Shifted Image');
  {$ifdef CASTLE_DESIGN_MODE}
  RegisterPropertyEditor(TypeInfo(AnsiString), TCastleShiftedImageControl, 'URL', TImageURLPropertyEditor);
  {$endif}
end.

