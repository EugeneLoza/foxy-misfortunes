{ This is a crop of deprecated TCastleTouchControl from Castle Game Engine }
{
  Copyright 2010-2018 Michalis Kamburelis.

  This file is part of "Castle Game Engine".

  "Castle Game Engine" is free software; see the file COPYING.txt,
  included in this distribution, for details about the copyright.

  "Castle Game Engine" is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  ----------------------------------------------------------------------------
}
unit TouchControlReimagined;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  CastleUiControls, CastleVectors, CastleKeysMouse;

type
  TTouchControlReimagined = class(TCastleUserInterface)
  strict private
    function MaxOffsetDist: Single;
  protected
    procedure PreferredSize(var PreferredWidth, PreferredHeight: Single); override;
  public
    FingerDragging: Integer; //< finger index that started drag, -1 if none
    FLeverOffset: TVector2;
    Scale: Single;
    constructor Create(AOwner: TComponent); override;
    procedure ReleaseMe;
    procedure Render; override;
    function Press(const Event: TInputPressRelease): boolean; override;
    function Release(const Event: TInputPressRelease): boolean; override;
    function Motion(const Event: TInputMotion): boolean; override;
  end;

implementation
uses
  CastleControls, CastleRectangles;

constructor TTouchControlReimagined.Create(AOwner: TComponent);
begin
  inherited;
  FingerDragging := -1;
end;

procedure TTouchControlReimagined.PreferredSize(var PreferredWidth, PreferredHeight: Single);
begin
  inherited;
  PreferredWidth  := Theme.ImagesPersistent[tiTouchCtlOuter].Width  * Scale * UIScale;
  PreferredHeight := Theme.ImagesPersistent[tiTouchCtlOuter].Height * Scale * UIScale;
end;

function TTouchControlReimagined.MaxOffsetDist: Single;
begin
  Result := Scale * UIScale *
    (Theme.ImagesPersistent[tiTouchCtlOuter].Width -
     Theme.ImagesPersistent[tiTouchCtlInner].Width) / 2;
end;

procedure TTouchControlReimagined.Render;
var
  LevOffsetTrimmedX, LevOffsetTrimmedY, MaxDist: Single;
  LeverDist: Double;
  InnerRect: TFloatRectangle;
  ImageInner, ImageOuter: TThemeImage;
  SR: TFloatRectangle;
begin
  inherited;
  SR := RenderRect;

  ImageInner := tiTouchCtlInner;
  ImageOuter := tiTouchCtlOuter;

  Theme.Draw(SR, ImageOuter, Scale * UIScale);

  // compute lever offset (must not move outside outer ring)
  LeverDist := FLeverOffset.Length;
  MaxDist := MaxOffsetDist;
  if LeverDist <= MaxDist then
  begin
    LevOffsetTrimmedX := FLeverOffset[0];
    LevOffsetTrimmedY := FLeverOffset[1];
  end else
  begin
    LevOffsetTrimmedX := (FLeverOffset[0] * MaxDist) / LeverDist;
    LevOffsetTrimmedY := (FLeverOffset[1] * MaxDist) / LeverDist;
  end;

  // draw lever
  InnerRect := FloatRectangle(Theme.ImagesPersistent[ImageInner].Image.Rect); // rectangle at (0,0)
  InnerRect.Width  := InnerRect.Width  * Scale * UIScale;
  InnerRect.Height := InnerRect.Height * Scale * UIScale;
  InnerRect.Left   := SR.Left   + (SR.Width  - InnerRect.Width ) / 2 + LevOffsetTrimmedX;
  InnerRect.Bottom := SR.Bottom + (SR.Height - InnerRect.Height) / 2 + LevOffsetTrimmedY;

  Theme.Draw(InnerRect, ImageInner, Scale * UIScale);
end;

function TTouchControlReimagined.Press(const Event: TInputPressRelease): boolean;
begin
  Result := inherited;
  if Result or (Event.EventType <> itMouseButton) then Exit;

  Result := ExclusiveEvents;
  FingerDragging := Event.FingerIndex;
  FLeverOffset := TVector2.Zero;
end;

procedure TTouchControlReimagined.ReleaseMe;
begin
  FingerDragging := -1;
  FLeverOffset := TVector2.Zero;
end;

function TTouchControlReimagined.Release(const Event: TInputPressRelease): boolean;
begin
  Result := inherited;
  if Result or (Event.EventType <> itMouseButton) then Exit;

  if FingerDragging = Event.FingerIndex then
  begin
    Result := ExclusiveEvents;

    ReleaseMe;
    VisibleChange([chRender]); { repaint with lever back in the center }
  end;
end;

function TTouchControlReimagined.Motion(const Event: TInputMotion): boolean;
begin
  Result := inherited;

  if (not Result) and (FingerDragging = Event.FingerIndex) then
  begin
    FLeverOffset := FLeverOffset + Event.Position - Event.OldPosition;
    VisibleChange([chRender]);
    Result := ExclusiveEvents;
  end;
end;

end.

