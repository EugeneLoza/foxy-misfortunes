{ Copyright (C) 2021-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }
unit GameMonsters;

{$mode ObjFPC}{$H+}

interface

uses
  Generics.Collections, DOM;

const
  DeathAnimationTime = 0.2;

type
  TMonsterInfo = class(TObject)
  public
    Id: Integer;
    Name: String;
    Description: String;
    DieSound: String;
    RenderTile: Integer;
    Speed: Single;
    RenderSize: Single;
    InteractionSize: Single;
    CollisionSize: Single;
    Reward: Integer;
    ShotKind: Integer;
    DecalKind: Integer;
    ActionCooldown: Single;
    constructor Create(const Element: TDOMElement);
  end;
  TMonstersList = specialize TObjectList<TMonsterInfo>;

var
  MonstersInfo: TMonstersList;

procedure InitMonsters;

implementation
uses
  SysUtils, CastleXmlUtils;

procedure InitMonsters;
var
  NewMonster: TMonsterInfo;
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
begin
  MonstersInfo := TMonstersList.Create(true);
  Doc := URLReadXML('castle-data:/map/monsters.xml');
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('monster');
    try
      while Iterator.GetNext do
      begin
        NewMonster := TMonsterInfo.Create(Iterator.Current);
        if NewMonster.Id <> MonstersInfo.Count then
          raise Exception.CreateFmt('Unexpected monster Id: %d (expected: %d) - All items must have unique IDs identical to their order', [NewMonster.Id, MonstersInfo.Count]);
        MonstersInfo.Add(NewMonster);
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;
end;

constructor TMonsterInfo.Create(const Element: TDOMElement);
begin
  //inherited - empty
  Id := Element.AttributeInteger('Id');
  Name := Element.AttributeString('Name');
  Description := Element.AttributeString('Description');
  DieSound := Element.AttributeString('DieSound');
  RenderTile := Element.AttributeInteger('RenderTile');
  RenderSize := Element.AttributeFloat('RenderSize');
  InteractionSize := Element.AttributeFloat('InteractionSize');
  Speed := Element.AttributeFloat('Speed');
  CollisionSize := Element.AttributeFloat('CollisionSize');
  Reward := Element.AttributeInteger('Reward');
  ShotKind := Element.AttributeInteger('ShotKind');
  DecalKind := Element.AttributeInteger('DecalKind');
  ActionCooldown := Element.AttributeFloat('ActionCooldown');
end;

finalization
  MonstersInfo.Free;
end.

